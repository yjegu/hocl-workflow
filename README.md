# Overview

GinFLow (aka HOCL-workflow) lets you run workflows using an distributed HOCL engine.

# Find more information

For further information, please refer to the documentation available on http://ginflow.inria.fr
