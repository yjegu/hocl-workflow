## Version 2.0.0

* Core refactored - introduced workflowModifier, workflowDeployer and workflowEnactor
* Add Embedded brokers 
* Add configuration through environment variables
* Add default configuration for distribution.home, distribution.name (if present in the current dir)
ssh.user

## Version 1.1.0

* Add adaptiveness feature.

## Version 1.0.0

* Fall back to default configuration in case config isn't found.

## Version 0.0.1

* Initial version
