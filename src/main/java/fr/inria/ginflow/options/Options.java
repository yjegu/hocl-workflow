/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.options;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;


/**
 * 
 * Options. TODO : singleton.
 * 
 * @author msimonin
 * 
 */
public class Options extends HashMap<String, String> {

    /** Defaul serial id. */
    private static final long serialVersionUID = 1L;

    /** The logger. */
    final static Logger log_ = LoggerFactory
            .getLogger(Options.class);
    
    /**
     * 
     * Mesos options.
     * 
     */
    /* Master address. */
    @key(value = "localhost:5050")
    public static final String MESOS_MASTER = "mesos.master";

    /** User to use when launching executors. */
    @key(value = "mesos")
    public static final String MESOS_USER = "mesos.user";
    
    /** cpu to use for all the services. */
    @key(value = "0.1")
    public static final String MESOS_RESOURCES_CPU = "mesos.resources.cpu";

    /** memory to use for all the services. */
    @key(value = "50")
    public static final String MESOS_RESOURCES_MEM = "mesos.resources.mem";
    /**
     * 
     * Ssh options
     * 
     */
    /* machine list. */
    @key(value = "localhost")
    public static final String SSH_MACHINES = "ssh.machines";

    @key(value = "")
    public static final String SSH_USERNAME = "ssh.user";

    @key(value = "~/.ssh/id_rsa")
    public static final String SSH_PRIVATE_KEY = "ssh.private.key";

    @key(value = "")
    public static final String SSH_PASSWORD = "ssh.password";

    /**
     * 
     * 
     * Broker options
     * 
     */
    @key(value = "activeMQ")
    public static final String BROKER = "broker";
    
    @key(value = "tcp://localhost:61616")
    public static final String ACTIVEMQ_BROKER_URL = "activemq.broker.url";

    @key(value = "localhost:9092")
    public static final String KAFKA_BOOTSTRAP_SERVERS = "kafka.bootstrap.servers";
    
    @key(value = "localhost:2181")
    public static final String KAFKA_ZOOKEEPER_CONNECT = "kafka.zookeeper.connect";
    
    @key(value = "false")
	public static final String STANDALONE_BROKER = "broker.standalone";
    
    /**
     * 
     * 
     * other options
     * 
     */
    /** Distribution home. */
    @key(value = "")
    public static final String DISTRIBUTION_HOME = "distribution.home";

    /** Distribution name. */
    @key(value = "hocl-workflow.jar")
    public static final String DISTRIBUTION_NAME = "distribution.name";

    /**
     * 
     * 
     * debug level
     * 
     * 
     */
    /** Debug level for GinFlow. */
    @key(value = "0")
    public static final String GINFLOW_DEBUG_LEVEL = "ginflow.debug";

    /** Debug level for Hocl. */
    @key(value = "0")
    public static final String HOCL_DEBUG_LEVEL = "hocl.debug";

    /** sleep chaos (time to wait before a portential kill).*/
    @key(value = "0")
    public static final String CHAOS_SLEEP = "chaos.sleep";

    /** threshold chaos.*/
    @key(value = "0")
    public static final String CHAOS_THRESHOLD = "chaos.threshold";


    
    /**
     *
     * Extract machines from a separated list of machines.
     *
     * @param string	coma separated list of machines.
     * @return The machine list.
     */
    public static ArrayList<String> extractMachines(String string) {
        return new ArrayList<>(Arrays.asList(string.replaceAll(" ", "").split(",")));
    }

    public Options() throws OptionException {
        super();
        // generate the map with default values.
        try {
            Field[] fields = Options.class.getDeclaredFields();
            for (Field field : fields) {
                if (field.isAnnotationPresent(key.class)) {
                    key annotation = (key) field.getAnnotation(key.class);
                    this.put((String) field.get(Options.class),
                            annotation.value());
                }
            }
        } catch (Exception exception) {
            throw new OptionException(exception);
        }
        
        
        
        updateWithEnvironment(System.getenv());
        // TODO : meta programm the following
        // exceptions :
        //
        // the distribution home.
        // if not set in the config, set it to GINFLOW_DISTRIBUTION_HOME.
        // otherwise keep it like it was.
        //
        // the ssh_key
        // if not set in the config, set it to GINFLOW_SSH_PRIVATE_KEY
        // otherwise keep it like it was.
//        if (get(Options.DISTRIBUTION_HOME).equals("") && System.getenv().get("GINFLOW_HOME") != null) {
//        	put(Options.DISTRIBUTION_HOME,System.getenv().get("GINFLOW_HOME"));
//        }
//        if (get(Options.SSH_PRIVATE_KEY).equals("") && System.getenv().get("GINFLOW_SSH_PRIVATE_KEY") != null) {
//        	put(Options.DISTRIBUTION_HOME,System.getenv().get("GINFLOW_HOME"));
//        }
    }

	public void updateWithEnvironment(Map<String, String> env) {
		
		// the distribution home is set to the current dir.
		put(Options.DISTRIBUTION_HOME, System.getProperty("user.dir"));
		put(Options.SSH_USERNAME, System.getProperty("user.name"));
		put(Options.SSH_PRIVATE_KEY, System.getProperty("user.home") + "/.ssh/id_rsa");
		// attempt to find the current jar 
		final Properties properties = new Properties();
		try {
			properties.load(this.getClass().getResourceAsStream("/project.properties"));
			final String jarName = String.format("%s-%s.jar",
    				properties.getProperty("artifactId"),
    				properties.getProperty("version"));
			File f = new File(System.getProperty("user.dir"));
			File[] matchingFiles = f.listFiles(new FilenameFilter() {
			    public boolean accept(File dir, String name) {
			        return name.equals(jarName);
			    }
			});
			if (matchingFiles.length == 1) {
				put(Options.DISTRIBUTION_NAME, matchingFiles[0].getName());
			}
		} catch (Exception e){
			log_.debug("Not able to find default jar names");
		}
		
		
		
		
		
		Map<String, String> environment = env;
        for ( java.util.Map.Entry<String, String> entry : environment.entrySet()) {
        	String key = entry.getKey();
        	String value = entry.getValue();
        	if (!key.startsWith("GINFLOW_")) {
        		continue;
        	}
        	String option = key.substring(8).toLowerCase().replace("_", ".");
        	put(option, value);
        }
	}

    /**
     * 
     * Build new options by merging with an existing one.
     * 
     * @param map               The map
     * @throws OptionException  The exception
     */
    public Options(Map<? extends String, ? extends String> map)
            throws OptionException {
        // and fill the missing values with default (merge the maps)
        this();
        this.putAll(map);
    }

    public static Options fromFile(String filename) throws OptionException, FileNotFoundException {
        // get the options from the config file
        Yaml yaml = new Yaml();

        InputStream input;
        Map<String, String> fileOptions;
        // default options
        Options options = new Options();
        input = new FileInputStream(new File(filename));
        fileOptions = (Map<String, String>) yaml.load(input);
        // override default
        options.putAll(fileOptions);
    
        return options;
    }
    
    /**
     * 
     * Serializes the option map to a command line suite of arguments
     * -o key1=value1 -o key2=value2
     * 
     * @return The string corresponding to all the options.
     */
    public String toCommandLine() {
        return Options.toCommandLine(this);
    }
    
    /**
     * 
     * Serializes the option map to a command line suite of arguments
     * -o key1=value1 -o key2=value2
     * 
     * @param options		map of options.
     * 
     * @return The string corresponding to all the options.
     */
    public static String toCommandLine(Map<String, String> options) {
        String s = "";
        for (java.util.Map.Entry<String, String> e : options.entrySet()) {
            s += String.format(" -o '%s=%s'", e.getKey(), e.getValue());
        }
        return s;
    }

	public boolean standaloneBroker() {
		return this.get(Options.STANDALONE_BROKER).equals("true");
	}
    
}
