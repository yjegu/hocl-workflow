/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.listener;

import fr.inria.ginflow.distributed.DestinationMolecule;
import fr.inria.ginflow.internal.api.GinflowTuple;
import fr.inria.ginflow.internal.api.Service;
import fr.inria.hocl.core.hocli.Molecule;

/**
 * 
 * Service listener.
 * 
 * @author msimonin
 * 
 */
public interface ServiceListener {

    
    /**
     * Called when the service has generated a GinflowType.RESULT
     * 
     * @param service	The service description
     */
    public void onResultReceived(Service service);

    /**
     * Called when the service sent his representation after a reduce.
     * 
     * @param destMol	The ddestination molecule
     */
    public void onServiceUpdated(DestinationMolecule destMol);

    
	/**
	 * Called when the service description has been sent.
	 * 
	 * @param service The service description
	 */
	public void onServiceDescriptionSent(GinflowTuple service);

}
