/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
// Edited manually after generation to transform the rule to a parameterized rule.
//
// let wa_trigger_dst324 = replace-one "T_3":<?w_3>
//                        by "T_3":<wa_update_src24, w_3> in
//
// becomes new Wa_trigger_dst("T3", "T2", "T4") programmatically

package fr.inria.ginflow.rules.centralised;
import fr.inria.hocl.core.hocli.*;

import java.io.*;

public class Wau_trigger_dst extends ReactionRule implements Serializable {

    /** The service to update.*/
	private String service_;
	
	/** The faulty service.*/
    private String faulty_;
    
    /** The alternative.*/
    private String alternative_;

    public Wau_trigger_dst(String service, String faulty, String alternative){
		super("wa_trigger_dst", Shot.ONE_SHOT);
		
		service_ = service;
		faulty_ = faulty;
		alternative_ = alternative;
		
		setTrope(Trope.OPTIMIZER);
			AtomIterator[] _HOCL_atomIteratorArray18;
		_HOCL_atomIteratorArray18 = new AtomIterator[1];
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple13;
			_HOCL_atomIteratorArrayTuple13 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal12 extends IteratorForExternal {
					public AtomIterator__HOCL_literal12(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator39 = (IteratorForTuple)permutation.getAtomIterator(0);
							String _HOCL_literal12 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator39.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal12.equals(service_);
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal12
				_HOCL_atomIteratorArrayTuple13[0] = new AtomIterator__HOCL_literal12();
			}
			{
				class IteratorSolution27 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray18;
						_HOCL_atomIteratorArray18 = new AtomIterator[0];
						
						MoleculeIterator[] _HOCL_moleculeIteratorArray18 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray18[0] = new MoleculeIterator(1); // w_3
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray18, _HOCL_moleculeIteratorArray18);
						return perm;
					}
				
				} // class IteratorSolution27
				_HOCL_atomIteratorArrayTuple13[1] = new IteratorSolution27();
			}
			_HOCL_atomIteratorArray18[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple13, Wau_trigger_dst.this);
		}
		MoleculeIterator[] _HOCL_moleculeIteratorArray19 = new MoleculeIterator[0];
		permutation = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray18, _HOCL_moleculeIteratorArray19);
	}
	public Wau_trigger_dst clone() {
		 return new Wau_trigger_dst(service_, faulty_, alternative_);
	}

	public void addType(String s){}

	// compute result of the rule wa_trigger_dst
	protected Molecule computeResult(){
		ExternalObject object;
		ReactionRule rule;
		String[] string;
		Tuple tuple;
		HoclList list;		
		IteratorForTuple _HOCL_tupleAtomIterator40 = (IteratorForTuple)permutation.getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution25 = (IteratorForSolution)_HOCL_tupleAtomIterator40.getAtomIterator(1);
		Molecule w_3 = _HOCL_iteratorSolution25.getSubPermutation().getMatchedMolecule(0);
		// new Molecule  
		Molecule mol18 = new Molecule();
		Tuple tuple13 = new Tuple(2);
		tuple13.set(0, ExternalObject.getHOCL_TypeTranslation(service_));
		// new solution + 0 
		Solution solution13 = new Solution(); 
		{
			// new Molecule  
			Molecule mol19 = new Molecule();
			rule = new Wau_update_src(faulty_, alternative_);
			mol19.add(rule);
			this.addType(rule.getName());
			
			
			mol19.add(w_3);
			solution13.addMolecule(mol19);
		}
		tuple13.set(1, solution13);
		tuple = tuple13;
		mol18.add(tuple);
		this.addType("Tuple");
		
		
		return mol18;
	}

} // end of class Wa_trigger_dst
