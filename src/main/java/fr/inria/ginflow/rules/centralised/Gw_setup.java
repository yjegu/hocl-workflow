/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.rules.centralised;
import fr.inria.ginflow.invoker.Invoker;
import fr.inria.hocl.core.hocli.*;
import java.io.*;

public class Gw_setup extends ReactionRule implements Serializable {

	
	public Gw_setup(){
		super("gw_setup", Shot.ONE_SHOT);
		setTrope(Trope.OPTIMIZER);
			AtomIterator[] _HOCL_atomIteratorArray0;
		_HOCL_atomIteratorArray0 = new AtomIterator[3];
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple0;
			_HOCL_atomIteratorArrayTuple0 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal0 extends IteratorForExternal {
					public AtomIterator__HOCL_literal0(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator0 = (IteratorForTuple)permutation.getAtomIterator(0);
							String _HOCL_literal0 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator0.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal0.equals("SRC");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal0
				_HOCL_atomIteratorArrayTuple0[0] = new AtomIterator__HOCL_literal0();
			}
			{
				class IteratorSolution1 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray0;
						_HOCL_atomIteratorArray0 = new AtomIterator[0];
						
						MoleculeIterator[] _HOCL_moleculeIteratorArray0 = new MoleculeIterator[0];
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray0, _HOCL_moleculeIteratorArray0);
						return perm;
					}
				
				} // class IteratorSolution1
				_HOCL_atomIteratorArrayTuple0[1] = new IteratorSolution1();
			}
			_HOCL_atomIteratorArray0[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple0, Gw_setup.this);
		}
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple1;
			_HOCL_atomIteratorArrayTuple1 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal1 extends IteratorForExternal {
					public AtomIterator__HOCL_literal1(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator1 = (IteratorForTuple)permutation.getAtomIterator(1);
							String _HOCL_literal1 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator1.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal1.equals("SRC_CONTROL");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal1
				_HOCL_atomIteratorArrayTuple1[0] = new AtomIterator__HOCL_literal1();
			}
			{
				class IteratorSolution3 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray1;
						_HOCL_atomIteratorArray1 = new AtomIterator[0];
						
						MoleculeIterator[] _HOCL_moleculeIteratorArray1 = new MoleculeIterator[0];
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray1, _HOCL_moleculeIteratorArray1);
						return perm;
					}
				
				} // class IteratorSolution3
				_HOCL_atomIteratorArrayTuple1[1] = new IteratorSolution3();
			}
			_HOCL_atomIteratorArray0[1] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple1, Gw_setup.this);
		}
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple2;
			_HOCL_atomIteratorArrayTuple2 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal2 extends IteratorForExternal {
					public AtomIterator__HOCL_literal2(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator2 = (IteratorForTuple)permutation.getAtomIterator(2);
							String _HOCL_literal2 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator2.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal2.equals("IN");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal2
				_HOCL_atomIteratorArrayTuple2[0] = new AtomIterator__HOCL_literal2();
			}
			{
				class IteratorSolution5 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray2;
						_HOCL_atomIteratorArray2 = new AtomIterator[0];
						
						MoleculeIterator[] _HOCL_moleculeIteratorArray2 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray2[0] = new MoleculeIterator(1); // w_in
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray2, _HOCL_moleculeIteratorArray2);
						return perm;
					}
				
				} // class IteratorSolution5
				_HOCL_atomIteratorArrayTuple2[1] = new IteratorSolution5();
			}
			_HOCL_atomIteratorArray0[2] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple2, Gw_setup.this);
		}
		MoleculeIterator[] _HOCL_moleculeIteratorArray3 = new MoleculeIterator[0];
		permutation = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray0, _HOCL_moleculeIteratorArray3);
	}
	public Gw_setup clone() {
		 return new Gw_setup();
	}

	public void addType(String s){}

	// compute result of the rule gw_setup
	protected Molecule computeResult(){
		ExternalObject object;
		ReactionRule rule;
		String[] string;
		Tuple tuple;
		HoclList list;		
		IteratorForTuple _HOCL_tupleAtomIterator3 = (IteratorForTuple)permutation.getAtomIterator(2);
		IteratorForSolution _HOCL_iteratorSolution0 = (IteratorForSolution)_HOCL_tupleAtomIterator3.getAtomIterator(1);
		Molecule w_in = _HOCL_iteratorSolution0.getSubPermutation().getMatchedMolecule(0);
		// new Molecule  
		Molecule mol0 = new Molecule();
		Tuple tuple0 = new Tuple(2);
		tuple0.set(0, ExternalObject.getHOCL_TypeTranslation("SRC"));
		// new solution + 0 
		Solution solution0 = new Solution(); 
		{
			// new Molecule  
			Molecule mol1 = new Molecule();
			solution0.addMolecule(mol1);
		}
		tuple0.set(1, solution0);
		tuple = tuple0;
		mol0.add(tuple);
		this.addType("Tuple");
		
		
		Tuple tuple1 = new Tuple(2);
		tuple1.set(0, ExternalObject.getHOCL_TypeTranslation("SRC_CONTROL"));
		// new solution + 0 
		Solution solution1 = new Solution(); 
		{
			// new Molecule  
			Molecule mol2 = new Molecule();
			solution1.addMolecule(mol2);
		}
		tuple1.set(1, solution1);
		tuple = tuple1;
		mol0.add(tuple);
		this.addType("Tuple");
		
		
		Tuple tuple2 = new Tuple(2);
		tuple2.set(0, ExternalObject.getHOCL_TypeTranslation("PAR"));
		// new solution + 0 
		Solution solution2 = new Solution(); 
		{
			// new Molecule  
			Molecule mol3 = new Molecule();
			mol3.add(w_in);
			solution2.addMolecule(mol3);
		}
		tuple2.set(1, solution2);
		tuple = tuple2;
		mol0.add(tuple);
		this.addType("Tuple");
		
		
		return mol0;
	}

} // end of class Gw_setup
