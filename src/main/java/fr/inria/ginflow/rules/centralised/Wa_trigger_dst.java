/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.rules.centralised;
import fr.inria.ginflow.invoker.Invoker;
import fr.inria.hocl.core.hocli.*;
import java.io.*;

public class Wa_trigger_dst extends ReactionRule implements Serializable {

	
	public Wa_trigger_dst(){
		super("wa_trigger_dst", Shot.ONE_SHOT);
		setTrope(Trope.OPTIMIZER);
			AtomIterator[] _HOCL_atomIteratorArray30;
		_HOCL_atomIteratorArray30 = new AtomIterator[1];
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple26;
			_HOCL_atomIteratorArrayTuple26 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal22 extends IteratorForExternal {
					public AtomIterator__HOCL_literal22(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator112 = (IteratorForTuple)permutation.getAtomIterator(0);
							String _HOCL_literal22 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator112.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal22.equals("T_3");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal22
				_HOCL_atomIteratorArrayTuple26[0] = new AtomIterator__HOCL_literal22();
			}
			{
				class IteratorSolution55 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray30;
						_HOCL_atomIteratorArray30 = new AtomIterator[1];
						{
							AtomIterator[] _HOCL_atomIteratorArrayTuple27;
							_HOCL_atomIteratorArrayTuple27 = new AtomIterator[2];
							{
								class AtomIterator__HOCL_literal23 extends IteratorForExternal {
									public AtomIterator__HOCL_literal23(){
										access = Access.REWRITE;
									}
									@Override
									public boolean conditionSatisfied() {
										Atom atom;
										boolean satisfied;
										atom = iterator.getElement();
										satisfied = false;
										if (atom instanceof ExternalObject
										  && ((ExternalObject)atom).getObject() instanceof String) {
											
											IteratorForTuple _HOCL_tupleAtomIterator113 = (IteratorForTuple)permutation.getAtomIterator(0);
											IteratorForSolution _HOCL_iteratorSolution84 = (IteratorForSolution)_HOCL_tupleAtomIterator113.getAtomIterator(1);
											IteratorForTuple _HOCL_tupleAtomIterator114 = (IteratorForTuple)_HOCL_iteratorSolution84.getSubPermutation().getAtomIterator(0);
											String _HOCL_literal23 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator114.getAtomIterator(0)).getObject();
											satisfied = _HOCL_literal23.equals("SRC");
										}
										return satisfied;
									}
								
								} // end of class AtomIterator__HOCL_literal23
								_HOCL_atomIteratorArrayTuple27[0] = new AtomIterator__HOCL_literal23();
							}
							{
								class IteratorSolution53 extends IteratorForSolution {
									protected Permutation makeSubPermutation(){
										Permutation perm;
										AtomIterator[] _HOCL_atomIteratorArray30;
										_HOCL_atomIteratorArray30 = new AtomIterator[0];
										
										MoleculeIterator[] _HOCL_moleculeIteratorArray30 = new MoleculeIterator[1];
										_HOCL_moleculeIteratorArray30[0] = new MoleculeIterator(1); // w_src
										
										perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray30, _HOCL_moleculeIteratorArray30);
										return perm;
									}
								
								} // class IteratorSolution53
								_HOCL_atomIteratorArrayTuple27[1] = new IteratorSolution53();
							}
							_HOCL_atomIteratorArray30[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple27, Wa_trigger_dst.this);
						}
						MoleculeIterator[] _HOCL_moleculeIteratorArray31 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray31[0] = new MoleculeIterator(1); // w_3
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray30, _HOCL_moleculeIteratorArray31);
						return perm;
					}
				
				} // class IteratorSolution55
				_HOCL_atomIteratorArrayTuple26[1] = new IteratorSolution55();
			}
			_HOCL_atomIteratorArray30[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple26, Wa_trigger_dst.this);
		}
		MoleculeIterator[] _HOCL_moleculeIteratorArray32 = new MoleculeIterator[0];
		permutation = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray30, _HOCL_moleculeIteratorArray32);
	}
	public Wa_trigger_dst clone() {
		 return new Wa_trigger_dst();
	}

	public void addType(String s){}

	// compute result of the rule wa_trigger_dst
	protected Molecule computeResult(){
		ExternalObject object;
		ReactionRule rule;
		String[] string;
		Tuple tuple;
		HoclList list;		
		IteratorForTuple _HOCL_tupleAtomIterator115 = (IteratorForTuple)permutation.getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution85 = (IteratorForSolution)_HOCL_tupleAtomIterator115.getAtomIterator(1);
		Molecule w_3 = _HOCL_iteratorSolution85.getSubPermutation().getMatchedMolecule(0);
		// new Molecule  
		Molecule mol30 = new Molecule();
		Tuple tuple27 = new Tuple(2);
		tuple27.set(0, ExternalObject.getHOCL_TypeTranslation("T_3"));
		// new solution + 0 
		Solution solution27 = new Solution(); 
		{
			// new Molecule  
			Molecule mol31 = new Molecule();
			Tuple tuple28 = new Tuple(2);
			tuple28.set(0, ExternalObject.getHOCL_TypeTranslation("SRC"));
			// new solution + 1 
			Solution solution26 = new Solution(); 
			{
				// new Molecule  
				Molecule mol32 = new Molecule();
				{ 
				Atom atomReference = ExternalObject.getHOCL_TypeTranslation("T_4");
				if (atomReference instanceof ExternalObject) {
					ExternalObject auxObject = (ExternalObject) atomReference;
					mol32.add(auxObject);
					string = auxObject.getObject().getClass().toString().split("\\.");
					this.addType(string[string.length-1]);
				} else {
					mol32.add(atomReference);
				if(atomReference instanceof Tuple) 
					this.addType("Tuple");
				}
				} 
				
				{ 
				Atom atomReference = ExternalObject.getHOCL_TypeTranslation("T_44");
				if (atomReference instanceof ExternalObject) {
					ExternalObject auxObject = (ExternalObject) atomReference;
					mol32.add(auxObject);
					string = auxObject.getObject().getClass().toString().split("\\.");
					this.addType(string[string.length-1]);
				} else {
					mol32.add(atomReference);
				if(atomReference instanceof Tuple) 
					this.addType("Tuple");
				}
				} 
				
				solution26.addMolecule(mol32);
			}
			tuple28.set(1, solution26);
			tuple = tuple28;
			mol31.add(tuple);
			this.addType("Tuple");
			
			
			mol31.add(w_3);
			solution27.addMolecule(mol31);
		}
		tuple27.set(1, solution27);
		tuple = tuple27;
		mol30.add(tuple);
		this.addType("Tuple");
		
		
		return mol30;
	}

} // end of class Wa_trigger_dst
