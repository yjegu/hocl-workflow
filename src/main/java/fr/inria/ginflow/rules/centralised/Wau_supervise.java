/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.rules.centralised;
import fr.inria.hocl.core.hocli.*;

import java.io.*;

public class Wau_supervise extends ReactionRule implements Serializable {

    /** The faulty service.*/
    private String faulty_;
    
    /** The alternative.*/
    private String alternative_;

    private String service_src_;

    private String service_dst_;
    
	
	public Wau_supervise(String service_src, String service_dst, String faulty, String alternative){
		super("supervise", Shot.ONE_SHOT);
		

        service_src_ = service_src;
        service_dst_ = service_dst;
        
        faulty_ = faulty;
        alternative_ = alternative;
        
		setTrope(Trope.EXPANSER);
			AtomIterator[] _HOCL_atomIteratorArray22;
		_HOCL_atomIteratorArray22 = new AtomIterator[1];
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple15;
			_HOCL_atomIteratorArrayTuple15 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal14 extends IteratorForExternal {
					public AtomIterator__HOCL_literal14(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator43 = (IteratorForTuple)permutation.getAtomIterator(0);
							String _HOCL_literal14 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator43.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal14.equals(faulty_);
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal14
				_HOCL_atomIteratorArrayTuple15[0] = new AtomIterator__HOCL_literal14();
			}
			{
				class IteratorSolution33 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray22;
						_HOCL_atomIteratorArray22 = new AtomIterator[1];
						{
							AtomIterator[] _HOCL_atomIteratorArrayTuple16;
							_HOCL_atomIteratorArrayTuple16 = new AtomIterator[2];
							{
								class AtomIterator__HOCL_literal15 extends IteratorForExternal {
									public AtomIterator__HOCL_literal15(){
										access = Access.REWRITE;
									}
									@Override
									public boolean conditionSatisfied() {
										Atom atom;
										boolean satisfied;
										atom = iterator.getElement();
										satisfied = false;
										if (atom instanceof ExternalObject
										  && ((ExternalObject)atom).getObject() instanceof String) {
											
											IteratorForTuple _HOCL_tupleAtomIterator44 = (IteratorForTuple)permutation.getAtomIterator(0);
											IteratorForSolution _HOCL_iteratorSolution27 = (IteratorForSolution)_HOCL_tupleAtomIterator44.getAtomIterator(1);
											IteratorForTuple _HOCL_tupleAtomIterator45 = (IteratorForTuple)_HOCL_iteratorSolution27.getSubPermutation().getAtomIterator(0);
											String _HOCL_literal15 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator45.getAtomIterator(0)).getObject();
											satisfied = _HOCL_literal15.equals("RES");
											System.out.println("RES ?" + atom);
										}
										return satisfied;
									}
								
								} // end of class AtomIterator__HOCL_literal15
								_HOCL_atomIteratorArrayTuple16[0] = new AtomIterator__HOCL_literal15();
							}
							{
								class IteratorSolution31 extends IteratorForSolution {
									protected Permutation makeSubPermutation(){
										Permutation perm;
										AtomIterator[] _HOCL_atomIteratorArray22;
										_HOCL_atomIteratorArray22 = new AtomIterator[1];
										{
											class AtomIterator_res extends IteratorForExternal {
												public AtomIterator_res(){
													access = Access.REWRITE;
												}
												@Override
												public boolean conditionSatisfied() {
													Atom atom;
													boolean satisfied;
													atom = iterator.getElement();
													satisfied = false;
													System.out.println("before if " + atom);
													if (atom instanceof ExternalObject
													  && ((ExternalObject)atom).getObject() instanceof String) {
														
														IteratorForTuple _HOCL_tupleAtomIterator46 = (IteratorForTuple)permutation.getAtomIterator(0);
														IteratorForSolution _HOCL_iteratorSolution28 = (IteratorForSolution)_HOCL_tupleAtomIterator46.getAtomIterator(1);
														IteratorForTuple _HOCL_tupleAtomIterator47 = (IteratorForTuple)_HOCL_iteratorSolution28.getSubPermutation().getAtomIterator(0);
														IteratorForSolution _HOCL_iteratorSolution29 = (IteratorForSolution)_HOCL_tupleAtomIterator47.getAtomIterator(1);
														String res = (String)((IteratorForExternal)_HOCL_iteratorSolution29.getSubPermutation().getAtomIterator(0)).getObject();
														//satisfied = (res.equals("command-2"));
														System.out.println("if");
														satisfied = true;
													}
													return satisfied;
												}
											
											} // end of class AtomIterator_res
											_HOCL_atomIteratorArray22[0] = new AtomIterator_res();
										}
										MoleculeIterator[] _HOCL_moleculeIteratorArray22 = new MoleculeIterator[0];
										perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray22, _HOCL_moleculeIteratorArray22);
										return perm;
									}
								
								} // class IteratorSolution31
								_HOCL_atomIteratorArrayTuple16[1] = new IteratorSolution31();
							}
							_HOCL_atomIteratorArray22[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple16, Wau_supervise.this);
						}
						MoleculeIterator[] _HOCL_moleculeIteratorArray23 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray23[0] = new MoleculeIterator(1); // w_2
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray22, _HOCL_moleculeIteratorArray23);
						return perm;
					}
				
				} // class IteratorSolution33
				_HOCL_atomIteratorArrayTuple15[1] = new IteratorSolution33();
			}
			_HOCL_atomIteratorArray22[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple15, Wau_supervise.this);
		}
		MoleculeIterator[] _HOCL_moleculeIteratorArray24 = new MoleculeIterator[1];
		_HOCL_moleculeIteratorArray24[0] = new MoleculeIterator(0);
		
		permutation = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray22, _HOCL_moleculeIteratorArray24);
	}
	public Wau_supervise clone() {
		 return new Wau_supervise(service_src_, service_dst_, faulty_, alternative_);
	}

	public void addType(String s){}

	// compute result of the rule supervise
	protected Molecule computeResult(){
		ExternalObject object;
		ReactionRule rule;
		String[] string;
		Tuple tuple;
		HoclList list;		
		Molecule w = permutation.getMatchedMolecule(0);
		IteratorForTuple _HOCL_tupleAtomIterator48 = (IteratorForTuple)permutation.getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution30 = (IteratorForSolution)_HOCL_tupleAtomIterator48.getAtomIterator(1);
		Molecule w_2 = _HOCL_iteratorSolution30.getSubPermutation().getMatchedMolecule(0);
		IteratorForTuple _HOCL_tupleAtomIterator49 = (IteratorForTuple)permutation.getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution31 = (IteratorForSolution)_HOCL_tupleAtomIterator49.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator50 = (IteratorForTuple)_HOCL_iteratorSolution31.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution32 = (IteratorForSolution)_HOCL_tupleAtomIterator50.getAtomIterator(1);
		String res = (String)((IteratorForExternal)_HOCL_iteratorSolution32.getSubPermutation().getAtomIterator(0)).getObject();
		// new Molecule  
		Molecule mol22 = new Molecule();
		Tuple tuple15 = new Tuple(2);
		tuple15.set(0, ExternalObject.getHOCL_TypeTranslation(faulty_));
		// new solution + 0 
		Solution solution16 = new Solution(); 
		{
			// new Molecule  
			Molecule mol23 = new Molecule();
			Tuple tuple16 = new Tuple(2);
			tuple16.set(0, ExternalObject.getHOCL_TypeTranslation("RES"));
			// new solution + 1 
			Solution solution15 = new Solution(); 
			{
				// new Molecule  
				Molecule mol24 = new Molecule();
				{ 
				Atom atomReference = ExternalObject.getHOCL_TypeTranslation(res);
				if (atomReference instanceof ExternalObject) {
					ExternalObject auxObject = (ExternalObject) atomReference;
					mol24.add(auxObject);
					string = auxObject.getObject().getClass().toString().split("\\.");
					this.addType(string[string.length-1]);
				} else {
					mol24.add(atomReference);
				if(atomReference instanceof Tuple) 
					this.addType("Tuple");
				}
				} 
				
				solution15.addMolecule(mol24);
			}
			tuple16.set(1, solution15);
			tuple = tuple16;
			mol23.add(tuple);
			this.addType("Tuple");
			
			
			mol23.add(w_2);
			solution16.addMolecule(mol23);
		}
		tuple15.set(1, solution16);
		tuple = tuple15;
		mol22.add(tuple);
		this.addType("Tuple");
		
		
		rule = new Wau_trigger_dst(service_dst_, faulty_, alternative_);
        mol22.add(rule);
		this.addType(rule.getName());
		
		
		rule = new Wau_trigger_src(service_src_, faulty_, alternative_);
		mol22.add(rule);
		this.addType(rule.getName());
		
		
		mol22.add(w);
		return mol22;
	}

} // end of class Supervise
