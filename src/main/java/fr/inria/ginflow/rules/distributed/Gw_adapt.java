/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.rules.distributed;
import fr.inria.ginflow.invoker.Invoker;
import fr.inria.ginflow.distributed.TransferMolecule;
import fr.inria.hocl.core.hocli.*;
import java.io.*;

public class Gw_adapt extends ReactionRule implements Serializable {

	
	public Gw_adapt(){
		super("gw_adapt", Shot.N_SHOT);
		setTrope(Trope.OPTIMIZER);
			AtomIterator[] _HOCL_atomIteratorArray10;
		_HOCL_atomIteratorArray10 = new AtomIterator[2];
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple8;
			_HOCL_atomIteratorArrayTuple8 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal8 extends IteratorForExternal {
					public AtomIterator__HOCL_literal8(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator13 = (IteratorForTuple)permutation.getAtomIterator(0);
							String _HOCL_literal8 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator13.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal8.equals("RESULT");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal8
				_HOCL_atomIteratorArrayTuple8[0] = new AtomIterator__HOCL_literal8();
			}
			{
				class IteratorSolution21 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray10;
						_HOCL_atomIteratorArray10 = new AtomIterator[1];
						{
							AtomIterator[] _HOCL_atomIteratorArrayTuple9;
							_HOCL_atomIteratorArrayTuple9 = new AtomIterator[2];
							{
								class AtomIterator__HOCL_literal9 extends IteratorForExternal {
									public AtomIterator__HOCL_literal9(){
										access = Access.REWRITE;
									}
									@Override
									public boolean conditionSatisfied() {
										Atom atom;
										boolean satisfied;
										atom = iterator.getElement();
										satisfied = false;
										if (atom instanceof ExternalObject
										  && ((ExternalObject)atom).getObject() instanceof String) {
											
											IteratorForTuple _HOCL_tupleAtomIterator14 = (IteratorForTuple)permutation.getAtomIterator(0);
											IteratorForSolution _HOCL_iteratorSolution5 = (IteratorForSolution)_HOCL_tupleAtomIterator14.getAtomIterator(1);
											IteratorForTuple _HOCL_tupleAtomIterator15 = (IteratorForTuple)_HOCL_iteratorSolution5.getSubPermutation().getAtomIterator(0);
											String _HOCL_literal9 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator15.getAtomIterator(0)).getObject();
											satisfied = _HOCL_literal9.equals("TRANSFER");
										}
										return satisfied;
									}
								
								} // end of class AtomIterator__HOCL_literal9
								_HOCL_atomIteratorArrayTuple9[0] = new AtomIterator__HOCL_literal9();
							}
							{
								class IteratorSolution19 extends IteratorForSolution {
									protected Permutation makeSubPermutation(){
										Permutation perm;
										AtomIterator[] _HOCL_atomIteratorArray10;
										_HOCL_atomIteratorArray10 = new AtomIterator[1];
										{
											AtomIterator[] _HOCL_atomIteratorArrayTuple10;
											_HOCL_atomIteratorArrayTuple10 = new AtomIterator[2];
											{
												class AtomIterator__HOCL_literal10 extends IteratorForExternal {
													public AtomIterator__HOCL_literal10(){
														access = Access.REWRITE;
													}
													@Override
													public boolean conditionSatisfied() {
														Atom atom;
														boolean satisfied;
														atom = iterator.getElement();
														satisfied = false;
														if (atom instanceof ExternalObject
														  && ((ExternalObject)atom).getObject() instanceof String) {
															
															IteratorForTuple _HOCL_tupleAtomIterator16 = (IteratorForTuple)permutation.getAtomIterator(0);
															IteratorForSolution _HOCL_iteratorSolution6 = (IteratorForSolution)_HOCL_tupleAtomIterator16.getAtomIterator(1);
															IteratorForTuple _HOCL_tupleAtomIterator17 = (IteratorForTuple)_HOCL_iteratorSolution6.getSubPermutation().getAtomIterator(0);
															IteratorForSolution _HOCL_iteratorSolution7 = (IteratorForSolution)_HOCL_tupleAtomIterator17.getAtomIterator(1);
															IteratorForTuple _HOCL_tupleAtomIterator18 = (IteratorForTuple)_HOCL_iteratorSolution7.getSubPermutation().getAtomIterator(0);
															String _HOCL_literal10 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator18.getAtomIterator(0)).getObject();
															satisfied = _HOCL_literal10.equals("EXIT");
														}
														return satisfied;
													}
												
												} // end of class AtomIterator__HOCL_literal10
												_HOCL_atomIteratorArrayTuple10[0] = new AtomIterator__HOCL_literal10();
											}
											{
												class IteratorSolution17 extends IteratorForSolution {
													protected Permutation makeSubPermutation(){
														Permutation perm;
														AtomIterator[] _HOCL_atomIteratorArray10;
														_HOCL_atomIteratorArray10 = new AtomIterator[1];
														{
															class AtomIterator_e extends IteratorForExternal {
																public AtomIterator_e(){
																	access = Access.REWRITE;
																}
																@Override
																public boolean conditionSatisfied() {
																	Atom atom;
																	boolean satisfied;
																	atom = iterator.getElement();
																	satisfied = false;
																	if (atom instanceof ExternalObject
																	  && ((ExternalObject)atom).getObject() instanceof Integer) {
																		satisfied = true;
																	}
																	return satisfied;
																}
															
															} // end of class AtomIterator_e
															_HOCL_atomIteratorArray10[0] = new AtomIterator_e();
														}
														MoleculeIterator[] _HOCL_moleculeIteratorArray10 = new MoleculeIterator[0];
														perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray10, _HOCL_moleculeIteratorArray10);
														return perm;
													}
												
												} // class IteratorSolution17
												_HOCL_atomIteratorArrayTuple10[1] = new IteratorSolution17();
											}
											_HOCL_atomIteratorArray10[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple10, Gw_adapt.this);
										}
										MoleculeIterator[] _HOCL_moleculeIteratorArray11 = new MoleculeIterator[1];
										_HOCL_moleculeIteratorArray11[0] = new MoleculeIterator(1); // w_transfer
										
										perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray10, _HOCL_moleculeIteratorArray11);
										return perm;
									}
								
								} // class IteratorSolution19
								_HOCL_atomIteratorArrayTuple9[1] = new IteratorSolution19();
							}
							_HOCL_atomIteratorArray10[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple9, Gw_adapt.this);
						}
						MoleculeIterator[] _HOCL_moleculeIteratorArray12 = new MoleculeIterator[0];
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray10, _HOCL_moleculeIteratorArray12);
						return perm;
					}
				
				} // class IteratorSolution21
				_HOCL_atomIteratorArrayTuple8[1] = new IteratorSolution21();
			}
			_HOCL_atomIteratorArray10[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple8, Gw_adapt.this);
		}
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple11;
			_HOCL_atomIteratorArrayTuple11 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal11 extends IteratorForExternal {
					public AtomIterator__HOCL_literal11(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator19 = (IteratorForTuple)permutation.getAtomIterator(1);
							String _HOCL_literal11 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator19.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal11.equals("ADAPT");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal11
				_HOCL_atomIteratorArrayTuple11[0] = new AtomIterator__HOCL_literal11();
			}
			{
				class IteratorSolution23 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray13;
						_HOCL_atomIteratorArray13 = new AtomIterator[0];
						
						MoleculeIterator[] _HOCL_moleculeIteratorArray13 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray13[0] = new MoleculeIterator(1); // w_adapt_dst
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray13, _HOCL_moleculeIteratorArray13);
						return perm;
					}
				
				} // class IteratorSolution23
				_HOCL_atomIteratorArrayTuple11[1] = new IteratorSolution23();
			}
			_HOCL_atomIteratorArray10[1] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple11, Gw_adapt.this);
		}
		MoleculeIterator[] _HOCL_moleculeIteratorArray14 = new MoleculeIterator[0];
		permutation = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray10, _HOCL_moleculeIteratorArray14);
	}
	public Gw_adapt clone() {
		 return new Gw_adapt();
	}

	public void addType(String s){}

	// compute result of the rule gw_adapt
	protected Molecule computeResult(){
		ExternalObject object;
		ReactionRule rule;
		String[] string;
		Tuple tuple;
		HoclList list;		
		IteratorForTuple _HOCL_tupleAtomIterator20 = (IteratorForTuple)permutation.getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution8 = (IteratorForSolution)_HOCL_tupleAtomIterator20.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator21 = (IteratorForTuple)_HOCL_iteratorSolution8.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution9 = (IteratorForSolution)_HOCL_tupleAtomIterator21.getAtomIterator(1);
		Molecule w_transfer = _HOCL_iteratorSolution9.getSubPermutation().getMatchedMolecule(0);
		IteratorForTuple _HOCL_tupleAtomIterator22 = (IteratorForTuple)permutation.getAtomIterator(1);
		IteratorForSolution _HOCL_iteratorSolution10 = (IteratorForSolution)_HOCL_tupleAtomIterator22.getAtomIterator(1);
		Molecule w_adapt_dst = _HOCL_iteratorSolution10.getSubPermutation().getMatchedMolecule(0);
		IteratorForTuple _HOCL_tupleAtomIterator23 = (IteratorForTuple)permutation.getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution11 = (IteratorForSolution)_HOCL_tupleAtomIterator23.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator24 = (IteratorForTuple)_HOCL_iteratorSolution11.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution12 = (IteratorForSolution)_HOCL_tupleAtomIterator24.getAtomIterator(1);
		IteratorForTuple _HOCL_tupleAtomIterator25 = (IteratorForTuple)_HOCL_iteratorSolution12.getSubPermutation().getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution13 = (IteratorForSolution)_HOCL_tupleAtomIterator25.getAtomIterator(1);
		Integer e = (Integer)((IteratorForExternal)_HOCL_iteratorSolution13.getSubPermutation().getAtomIterator(0)).getObject();
		// new Molecule  
		Molecule mol10 = new Molecule();
		Tuple tuple9 = new Tuple(2);
		tuple9.set(0, ExternalObject.getHOCL_TypeTranslation("TRF"));
		tuple9.set(1, ExternalObject.getHOCL_TypeTranslation(TransferMolecule.put("ADAPT", w_adapt_dst, "ADAPT")));
		tuple = tuple9;
		mol10.add(tuple);
		this.addType("Tuple");
		
		
		Tuple tuple10 = new Tuple(2);
		tuple10.set(0, ExternalObject.getHOCL_TypeTranslation("RESULT"));
		// new solution + 0 
		Solution solution10 = new Solution(); 
		{
			// new Molecule  
			Molecule mol11 = new Molecule();
			Tuple tuple11 = new Tuple(2);
			tuple11.set(0, ExternalObject.getHOCL_TypeTranslation("TRANSFER"));
			// new solution + 1 
			Solution solution9 = new Solution(); 
			{
				// new Molecule  
				Molecule mol12 = new Molecule();
				Tuple tuple12 = new Tuple(2);
				tuple12.set(0, ExternalObject.getHOCL_TypeTranslation("EXIT"));
				// new solution + 2 
				Solution solution8 = new Solution(); 
				{
					// new Molecule  
					Molecule mol13 = new Molecule();
					{ 
					Atom atomReference = ExternalObject.getHOCL_TypeTranslation(e);
					if (atomReference instanceof ExternalObject) {
						ExternalObject auxObject = (ExternalObject) atomReference;
						mol13.add(auxObject);
						string = auxObject.getObject().getClass().toString().split("\\.");
						this.addType(string[string.length-1]);
					} else {
						mol13.add(atomReference);
					if(atomReference instanceof Tuple) 
						this.addType("Tuple");
					}
					} 
					
					solution8.addMolecule(mol13);
				}
				tuple12.set(1, solution8);
				tuple = tuple12;
				mol12.add(tuple);
				this.addType("Tuple");
				
				
				mol12.add(w_transfer);
				solution9.addMolecule(mol12);
			}
			tuple11.set(1, solution9);
			tuple = tuple11;
			mol11.add(tuple);
			this.addType("Tuple");
			
			
			solution10.addMolecule(mol11);
		}
		tuple10.set(1, solution10);
		tuple = tuple10;
		mol10.add(tuple);
		this.addType("Tuple");
		
		
		return mol10;
	}

} // end of class Gw_adapt
