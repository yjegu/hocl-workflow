/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.rules.distributed;
import fr.inria.ginflow.invoker.Invoker;
import fr.inria.ginflow.distributed.TransferMolecule;
import fr.inria.hocl.core.hocli.*;
import java.io.*;

public class Gw_swap_dst extends ReactionRule implements Serializable {

	
	public Gw_swap_dst(){
		super("gw_swap_dst", Shot.ONE_SHOT);
		setTrope(Trope.REDUCER);
			AtomIterator[] _HOCL_atomIteratorArray42;
		_HOCL_atomIteratorArray42 = new AtomIterator[5];
		{
			class AtomIterator__HOCL_literal33 extends IteratorForExternal {
				public AtomIterator__HOCL_literal33(){
					access = Access.REWRITE;
				}
				@Override
				public boolean conditionSatisfied() {
					Atom atom;
					boolean satisfied;
					atom = iterator.getElement();
					satisfied = false;
					if (atom instanceof ExternalObject
					  && ((ExternalObject)atom).getObject() instanceof String) {
						
						String _HOCL_literal33 = (String)((IteratorForExternal)permutation.getAtomIterator(0)).getObject();
						satisfied = _HOCL_literal33.equals("ADAPT");
					}
					return satisfied;
				}
			
			} // end of class AtomIterator__HOCL_literal33
			_HOCL_atomIteratorArray42[0] = new AtomIterator__HOCL_literal33();
		}
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple33;
			_HOCL_atomIteratorArrayTuple33 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal34 extends IteratorForExternal {
					public AtomIterator__HOCL_literal34(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator89 = (IteratorForTuple)permutation.getAtomIterator(1);
							String _HOCL_literal34 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator89.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal34.equals("ADAPT_DST");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal34
				_HOCL_atomIteratorArrayTuple33[0] = new AtomIterator__HOCL_literal34();
			}
			{
				class IteratorSolution67 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray42;
						_HOCL_atomIteratorArray42 = new AtomIterator[0];
						
						MoleculeIterator[] _HOCL_moleculeIteratorArray42 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray42[0] = new MoleculeIterator(1); // w_adapt_dst
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray42, _HOCL_moleculeIteratorArray42);
						return perm;
					}
				
				} // class IteratorSolution67
				_HOCL_atomIteratorArrayTuple33[1] = new IteratorSolution67();
			}
			_HOCL_atomIteratorArray42[1] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple33, Gw_swap_dst.this);
		}
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple34;
			_HOCL_atomIteratorArrayTuple34 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal35 extends IteratorForExternal {
					public AtomIterator__HOCL_literal35(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator90 = (IteratorForTuple)permutation.getAtomIterator(2);
							String _HOCL_literal35 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator90.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal35.equals("DST");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal35
				_HOCL_atomIteratorArrayTuple34[0] = new AtomIterator__HOCL_literal35();
			}
			{
				class IteratorSolution69 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray43;
						_HOCL_atomIteratorArray43 = new AtomIterator[0];
						
						MoleculeIterator[] _HOCL_moleculeIteratorArray43 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray43[0] = new MoleculeIterator(1); // w_dst
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray43, _HOCL_moleculeIteratorArray43);
						return perm;
					}
				
				} // class IteratorSolution69
				_HOCL_atomIteratorArrayTuple34[1] = new IteratorSolution69();
			}
			_HOCL_atomIteratorArray42[2] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple34, Gw_swap_dst.this);
		}
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple35;
			_HOCL_atomIteratorArrayTuple35 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal36 extends IteratorForExternal {
					public AtomIterator__HOCL_literal36(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator91 = (IteratorForTuple)permutation.getAtomIterator(3);
							String _HOCL_literal36 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator91.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal36.equals("ADAPT_DST_CONTROL");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal36
				_HOCL_atomIteratorArrayTuple35[0] = new AtomIterator__HOCL_literal36();
			}
			{
				class IteratorSolution71 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray44;
						_HOCL_atomIteratorArray44 = new AtomIterator[0];
						
						MoleculeIterator[] _HOCL_moleculeIteratorArray44 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray44[0] = new MoleculeIterator(1); // w_adapt_dst_control
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray44, _HOCL_moleculeIteratorArray44);
						return perm;
					}
				
				} // class IteratorSolution71
				_HOCL_atomIteratorArrayTuple35[1] = new IteratorSolution71();
			}
			_HOCL_atomIteratorArray42[3] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple35, Gw_swap_dst.this);
		}
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple36;
			_HOCL_atomIteratorArrayTuple36 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal37 extends IteratorForExternal {
					public AtomIterator__HOCL_literal37(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator92 = (IteratorForTuple)permutation.getAtomIterator(4);
							String _HOCL_literal37 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator92.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal37.equals("DST_CONTROL");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal37
				_HOCL_atomIteratorArrayTuple36[0] = new AtomIterator__HOCL_literal37();
			}
			{
				class IteratorSolution73 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray45;
						_HOCL_atomIteratorArray45 = new AtomIterator[0];
						
						MoleculeIterator[] _HOCL_moleculeIteratorArray45 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray45[0] = new MoleculeIterator(1); // w_dst_control
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray45, _HOCL_moleculeIteratorArray45);
						return perm;
					}
				
				} // class IteratorSolution73
				_HOCL_atomIteratorArrayTuple36[1] = new IteratorSolution73();
			}
			_HOCL_atomIteratorArray42[4] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple36, Gw_swap_dst.this);
		}
		MoleculeIterator[] _HOCL_moleculeIteratorArray46 = new MoleculeIterator[0];
		permutation = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray42, _HOCL_moleculeIteratorArray46);
	}
	public Gw_swap_dst clone() {
		 return new Gw_swap_dst();
	}

	public void addType(String s){}

	// compute result of the rule gw_swap_dst
	protected Molecule computeResult(){
		ExternalObject object;
		ReactionRule rule;
		String[] string;
		Tuple tuple;
		HoclList list;		
		IteratorForTuple _HOCL_tupleAtomIterator93 = (IteratorForTuple)permutation.getAtomIterator(3);
		IteratorForSolution _HOCL_iteratorSolution56 = (IteratorForSolution)_HOCL_tupleAtomIterator93.getAtomIterator(1);
		Molecule w_adapt_dst_control = _HOCL_iteratorSolution56.getSubPermutation().getMatchedMolecule(0);
		IteratorForTuple _HOCL_tupleAtomIterator94 = (IteratorForTuple)permutation.getAtomIterator(1);
		IteratorForSolution _HOCL_iteratorSolution57 = (IteratorForSolution)_HOCL_tupleAtomIterator94.getAtomIterator(1);
		Molecule w_adapt_dst = _HOCL_iteratorSolution57.getSubPermutation().getMatchedMolecule(0);
		// new Molecule  
		Molecule mol39 = new Molecule();
		Tuple tuple34 = new Tuple(2);
		tuple34.set(0, ExternalObject.getHOCL_TypeTranslation("DST"));
		// new solution + 0 
		Solution solution30 = new Solution(); 
		{
			// new Molecule  
			Molecule mol40 = new Molecule();
			mol40.add(w_adapt_dst);
			solution30.addMolecule(mol40);
		}
		tuple34.set(1, solution30);
		tuple = tuple34;
		mol39.add(tuple);
		this.addType("Tuple");
		
		
		Tuple tuple35 = new Tuple(2);
		tuple35.set(0, ExternalObject.getHOCL_TypeTranslation("DST_CONTROL"));
		// new solution + 0 
		Solution solution31 = new Solution(); 
		{
			// new Molecule  
			Molecule mol41 = new Molecule();
			mol41.add(w_adapt_dst_control);
			solution31.addMolecule(mol41);
		}
		tuple35.set(1, solution31);
		tuple = tuple35;
		mol39.add(tuple);
		this.addType("Tuple");
		
		
		return mol39;
	}

} // end of class Gw_swap_dst
