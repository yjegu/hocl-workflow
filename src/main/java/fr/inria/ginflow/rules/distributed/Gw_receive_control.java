/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.rules.distributed;
import fr.inria.ginflow.invoker.Invoker;
import fr.inria.ginflow.distributed.TransferMolecule;
import fr.inria.hocl.core.hocli.*;
import java.io.*;

public class Gw_receive_control extends ReactionRule implements Serializable {

	
	public Gw_receive_control(){
		super("gw_receive_control", Shot.N_SHOT);
		setTrope(Trope.REDUCER);
			AtomIterator[] _HOCL_atomIteratorArray36;
		_HOCL_atomIteratorArray36 = new AtomIterator[2];
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple28;
			_HOCL_atomIteratorArrayTuple28 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal28 extends IteratorForExternal {
					public AtomIterator__HOCL_literal28(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator77 = (IteratorForTuple)permutation.getAtomIterator(0);
							String _HOCL_literal28 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator77.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal28.equals("SRC_CONTROL");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal28
				_HOCL_atomIteratorArrayTuple28[0] = new AtomIterator__HOCL_literal28();
			}
			{
				class IteratorSolution57 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray36;
						_HOCL_atomIteratorArray36 = new AtomIterator[1];
						{
							class AtomIterator_provenance extends IteratorForExternal {
								public AtomIterator_provenance(){
									access = Access.REWRITE;
								}
								@Override
								public boolean conditionSatisfied() {
									Atom atom;
									boolean satisfied;
									atom = iterator.getElement();
									satisfied = false;
									if (atom instanceof ExternalObject
									  && ((ExternalObject)atom).getObject() instanceof String) {
										satisfied = true;
									}
									return satisfied;
								}
							
							} // end of class AtomIterator_provenance
							_HOCL_atomIteratorArray36[0] = new AtomIterator_provenance();
						}
						MoleculeIterator[] _HOCL_moleculeIteratorArray36 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray36[0] = new MoleculeIterator(1); // w_src
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray36, _HOCL_moleculeIteratorArray36);
						return perm;
					}
				
				} // class IteratorSolution57
				_HOCL_atomIteratorArrayTuple28[1] = new IteratorSolution57();
			}
			_HOCL_atomIteratorArray36[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple28, Gw_receive_control.this);
		}
		{
			AtomIterator[] _HOCL_atomIteratorArrayTuple29;
			_HOCL_atomIteratorArrayTuple29 = new AtomIterator[2];
			{
				class AtomIterator__HOCL_literal29 extends IteratorForExternal {
					public AtomIterator__HOCL_literal29(){
						access = Access.REWRITE;
					}
					@Override
					public boolean conditionSatisfied() {
						Atom atom;
						boolean satisfied;
						atom = iterator.getElement();
						satisfied = false;
						if (atom instanceof ExternalObject
						  && ((ExternalObject)atom).getObject() instanceof String) {
							
							IteratorForTuple _HOCL_tupleAtomIterator78 = (IteratorForTuple)permutation.getAtomIterator(1);
							String _HOCL_literal29 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator78.getAtomIterator(0)).getObject();
							satisfied = _HOCL_literal29.equals("TRANSFER");
						}
						return satisfied;
					}
				
				} // end of class AtomIterator__HOCL_literal29
				_HOCL_atomIteratorArrayTuple29[0] = new AtomIterator__HOCL_literal29();
			}
			{
				class IteratorSolution65 extends IteratorForSolution {
					protected Permutation makeSubPermutation(){
						Permutation perm;
						AtomIterator[] _HOCL_atomIteratorArray37;
						_HOCL_atomIteratorArray37 = new AtomIterator[3];
						{
							AtomIterator[] _HOCL_atomIteratorArrayTuple30;
							_HOCL_atomIteratorArrayTuple30 = new AtomIterator[2];
							{
								class AtomIterator__HOCL_literal30 extends IteratorForExternal {
									public AtomIterator__HOCL_literal30(){
										access = Access.REWRITE;
									}
									@Override
									public boolean conditionSatisfied() {
										Atom atom;
										boolean satisfied;
										atom = iterator.getElement();
										satisfied = false;
										if (atom instanceof ExternalObject
										  && ((ExternalObject)atom).getObject() instanceof String) {
											
											IteratorForTuple _HOCL_tupleAtomIterator79 = (IteratorForTuple)permutation.getAtomIterator(1);
											IteratorForSolution _HOCL_iteratorSolution49 = (IteratorForSolution)_HOCL_tupleAtomIterator79.getAtomIterator(1);
											IteratorForTuple _HOCL_tupleAtomIterator80 = (IteratorForTuple)_HOCL_iteratorSolution49.getSubPermutation().getAtomIterator(0);
											String _HOCL_literal30 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator80.getAtomIterator(0)).getObject();
											satisfied = _HOCL_literal30.equals("OUT");
										}
										return satisfied;
									}
								
								} // end of class AtomIterator__HOCL_literal30
								_HOCL_atomIteratorArrayTuple30[0] = new AtomIterator__HOCL_literal30();
							}
							{
								class IteratorSolution59 extends IteratorForSolution {
									protected Permutation makeSubPermutation(){
										Permutation perm;
										AtomIterator[] _HOCL_atomIteratorArray37;
										_HOCL_atomIteratorArray37 = new AtomIterator[0];
										
										MoleculeIterator[] _HOCL_moleculeIteratorArray37 = new MoleculeIterator[1];
										_HOCL_moleculeIteratorArray37[0] = new MoleculeIterator(1); // w_out
										
										perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray37, _HOCL_moleculeIteratorArray37);
										return perm;
									}
								
								} // class IteratorSolution59
								_HOCL_atomIteratorArrayTuple30[1] = new IteratorSolution59();
							}
							_HOCL_atomIteratorArray37[0] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple30, Gw_receive_control.this);
						}
						{
							AtomIterator[] _HOCL_atomIteratorArrayTuple31;
							_HOCL_atomIteratorArrayTuple31 = new AtomIterator[2];
							{
								class AtomIterator__HOCL_literal31 extends IteratorForExternal {
									public AtomIterator__HOCL_literal31(){
										access = Access.REWRITE;
									}
									@Override
									public boolean conditionSatisfied() {
										Atom atom;
										boolean satisfied;
										atom = iterator.getElement();
										satisfied = false;
										if (atom instanceof ExternalObject
										  && ((ExternalObject)atom).getObject() instanceof String) {
											
											IteratorForTuple _HOCL_tupleAtomIterator81 = (IteratorForTuple)permutation.getAtomIterator(1);
											IteratorForSolution _HOCL_iteratorSolution50 = (IteratorForSolution)_HOCL_tupleAtomIterator81.getAtomIterator(1);
											IteratorForTuple _HOCL_tupleAtomIterator82 = (IteratorForTuple)_HOCL_iteratorSolution50.getSubPermutation().getAtomIterator(1);
											String _HOCL_literal31 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator82.getAtomIterator(0)).getObject();
											satisfied = _HOCL_literal31.equals("NAME");
										}
										return satisfied;
									}
								
								} // end of class AtomIterator__HOCL_literal31
								_HOCL_atomIteratorArrayTuple31[0] = new AtomIterator__HOCL_literal31();
							}
							{
								class IteratorSolution61 extends IteratorForSolution {
									protected Permutation makeSubPermutation(){
										Permutation perm;
										AtomIterator[] _HOCL_atomIteratorArray38;
										_HOCL_atomIteratorArray38 = new AtomIterator[1];
										{
											class AtomIterator_s extends IteratorForExternal {
												public AtomIterator_s(){
													access = Access.REWRITE;
												}
												@Override
												public boolean conditionSatisfied() {
													Atom atom;
													boolean satisfied;
													atom = iterator.getElement();
													satisfied = false;
													if (atom instanceof ExternalObject
													  && ((ExternalObject)atom).getObject() instanceof String) {
														satisfied = true;
													}
													return satisfied;
												}
											
											} // end of class AtomIterator_s
											_HOCL_atomIteratorArray38[0] = new AtomIterator_s();
										}
										MoleculeIterator[] _HOCL_moleculeIteratorArray38 = new MoleculeIterator[0];
										perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray38, _HOCL_moleculeIteratorArray38);
										return perm;
									}
								
								} // class IteratorSolution61
								_HOCL_atomIteratorArrayTuple31[1] = new IteratorSolution61();
							}
							_HOCL_atomIteratorArray37[1] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple31, Gw_receive_control.this);
						}
						{
							AtomIterator[] _HOCL_atomIteratorArrayTuple32;
							_HOCL_atomIteratorArrayTuple32 = new AtomIterator[2];
							{
								class AtomIterator__HOCL_literal32 extends IteratorForExternal {
									public AtomIterator__HOCL_literal32(){
										access = Access.REWRITE;
									}
									@Override
									public boolean conditionSatisfied() {
										Atom atom;
										boolean satisfied;
										atom = iterator.getElement();
										satisfied = false;
										if (atom instanceof ExternalObject
										  && ((ExternalObject)atom).getObject() instanceof String) {
											
											IteratorForTuple _HOCL_tupleAtomIterator83 = (IteratorForTuple)permutation.getAtomIterator(1);
											IteratorForSolution _HOCL_iteratorSolution51 = (IteratorForSolution)_HOCL_tupleAtomIterator83.getAtomIterator(1);
											IteratorForTuple _HOCL_tupleAtomIterator84 = (IteratorForTuple)_HOCL_iteratorSolution51.getSubPermutation().getAtomIterator(2);
											String _HOCL_literal32 = (String)((IteratorForExternal)_HOCL_tupleAtomIterator84.getAtomIterator(0)).getObject();
											satisfied = _HOCL_literal32.equals("EXIT");
										}
										return satisfied;
									}
								
								} // end of class AtomIterator__HOCL_literal32
								_HOCL_atomIteratorArrayTuple32[0] = new AtomIterator__HOCL_literal32();
							}
							{
								class IteratorSolution63 extends IteratorForSolution {
									protected Permutation makeSubPermutation(){
										Permutation perm;
										AtomIterator[] _HOCL_atomIteratorArray39;
										_HOCL_atomIteratorArray39 = new AtomIterator[1];
										{
											class AtomIterator_e extends IteratorForExternal {
												public AtomIterator_e(){
													access = Access.REWRITE;
												}
												@Override
												public boolean conditionSatisfied() {
													Atom atom;
													boolean satisfied;
													atom = iterator.getElement();
													satisfied = false;
													if (atom instanceof ExternalObject
													  && ((ExternalObject)atom).getObject() instanceof Integer) {
														
														IteratorForTuple _HOCL_tupleAtomIterator85 = (IteratorForTuple)permutation.getAtomIterator(1);
														IteratorForSolution _HOCL_iteratorSolution52 = (IteratorForSolution)_HOCL_tupleAtomIterator85.getAtomIterator(1);
														IteratorForTuple _HOCL_tupleAtomIterator86 = (IteratorForTuple)_HOCL_iteratorSolution52.getSubPermutation().getAtomIterator(1);
														IteratorForSolution _HOCL_iteratorSolution53 = (IteratorForSolution)_HOCL_tupleAtomIterator86.getAtomIterator(1);
														String s = (String)((IteratorForExternal)_HOCL_iteratorSolution53.getSubPermutation().getAtomIterator(0)).getObject();
														IteratorForTuple _HOCL_tupleAtomIterator87 = (IteratorForTuple)permutation.getAtomIterator(0);
														IteratorForSolution _HOCL_iteratorSolution54 = (IteratorForSolution)_HOCL_tupleAtomIterator87.getAtomIterator(1);
														String provenance = (String)((IteratorForExternal)_HOCL_iteratorSolution54.getSubPermutation().getAtomIterator(0)).getObject();
														satisfied = (s.equals(provenance));
													}
													return satisfied;
												}
											
											} // end of class AtomIterator_e
											_HOCL_atomIteratorArray39[0] = new AtomIterator_e();
										}
										MoleculeIterator[] _HOCL_moleculeIteratorArray39 = new MoleculeIterator[0];
										perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray39, _HOCL_moleculeIteratorArray39);
										return perm;
									}
								
								} // class IteratorSolution63
								_HOCL_atomIteratorArrayTuple32[1] = new IteratorSolution63();
							}
							_HOCL_atomIteratorArray37[2] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple32, Gw_receive_control.this);
						}
						MoleculeIterator[] _HOCL_moleculeIteratorArray40 = new MoleculeIterator[1];
						_HOCL_moleculeIteratorArray40[0] = new MoleculeIterator(1); // w_transfer
						
						perm = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray37, _HOCL_moleculeIteratorArray40);
						return perm;
					}
				
				} // class IteratorSolution65
				_HOCL_atomIteratorArrayTuple29[1] = new IteratorSolution65();
			}
			_HOCL_atomIteratorArray36[1] = new IteratorForTuple(_HOCL_atomIteratorArrayTuple29, Gw_receive_control.this);
		}
		MoleculeIterator[] _HOCL_moleculeIteratorArray41 = new MoleculeIterator[0];
		permutation = newPermutation(PermutationType.SOLUTION, _HOCL_atomIteratorArray36, _HOCL_moleculeIteratorArray41);
	}
	public Gw_receive_control clone() {
		 return new Gw_receive_control();
	}

	public void addType(String s){}

	// compute result of the rule gw_receive_control
	protected Molecule computeResult(){
		ExternalObject object;
		ReactionRule rule;
		String[] string;
		Tuple tuple;
		HoclList list;		
		IteratorForTuple _HOCL_tupleAtomIterator88 = (IteratorForTuple)permutation.getAtomIterator(0);
		IteratorForSolution _HOCL_iteratorSolution55 = (IteratorForSolution)_HOCL_tupleAtomIterator88.getAtomIterator(1);
		Molecule w_src = _HOCL_iteratorSolution55.getSubPermutation().getMatchedMolecule(0);
		// new Molecule  
		Molecule mol37 = new Molecule();
		Tuple tuple33 = new Tuple(2);
		tuple33.set(0, ExternalObject.getHOCL_TypeTranslation("SRC_CONTROL"));
		// new solution + 0 
		Solution solution29 = new Solution(); 
		{
			// new Molecule  
			Molecule mol38 = new Molecule();
			mol38.add(w_src);
			solution29.addMolecule(mol38);
		}
		tuple33.set(1, solution29);
		tuple = tuple33;
		mol37.add(tuple);
		this.addType("Tuple");
		
		
		return mol37;
	}

} // end of class Gw_receive_control
