/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.distributed;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.hocl.core.hocli.Molecule;
import fr.inria.hocl.core.hocli.Solution;

/**
 * 
 * Treat in batch incoming molecules batch shouldn't be too large to avoid
 * combinatory explosion.
 * 
 * @author msimonin
 * 
 */
public class PendingMoleculeConsumer extends Thread {

    /** The logger. */
    final static Logger log = LoggerFactory
            .getLogger(PendingMoleculeConsumer.class);

    private Solution sol;
    private Object reduceReady;

    private BlockingQueue<Molecule> pendingMolecules;

    private int max = 20;

    public PendingMoleculeConsumer(Solution sol, Object lockObject,
            BlockingQueue<Molecule> pendingMolecules) {
        log.debug("Creating new pendingMoleculeConsumer");
        this.sol = sol;
        this.reduceReady = lockObject;
        this.pendingMolecules = pendingMolecules;
    }

    public void run() {
        int size;
        List<Molecule> molecules = new ArrayList<Molecule>();
        try {
            while (true) {
                log.debug("Waiting new batch");
                molecules = new ArrayList<Molecule>();
                Molecule molecule = pendingMolecules.take();

                molecules.add(molecule);
                // we should not take all the pending molecule
                // -> avoid the time explosion.
                for (int i = 0; i < max; i++) {
                    molecule = pendingMolecules.poll();
                    if (molecule != null) {
                        molecules.add(molecule);
                    }
                }

                log.debug(String.format(
                        "Ready to add %d molecules to the solution",
                        molecules.size()));
                synchronized (reduceReady) {
                    while (ReduceSolutionFlag.reduce) {
                        reduceReady.wait();
                    }

                    for (Molecule mole : molecules) {
                        sol.addMolecule(mole);
                    }
                    // notify the service invoker routine.
                    ReduceSolutionFlag.reduce = true;
                    // this is asynchronous :
                    // meaning this thread can be pass here before a reduction
                    // occurs.
                    reduceReady.notify();
                }
            }
        } catch (Exception e) {
            log.error("FATAL error", e);
        }
    }
}
