/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.distributed.communication.api.impl.kafka;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;

import kafka.consumer.Consumer;
import kafka.consumer.ConsumerConfig;
import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;
import kafka.message.MessageAndMetadata;
import kafka.serializer.StringDecoder;
import fr.inria.ginflow.options.Options;
import fr.inria.hocl.core.hocli.Molecule;

/**
 * 
 * Listens for incoming solution (service invoker side).
 * Add them to reduce queue.
 * 
 * @author msimonin
 *
 */
public class MoleculeListener extends Thread {

    
    /** The workflow Id.*/
    private String workflowId_;
    
    /** The options.*/
    private Map<String, String> options_;
    
    private ConsumerConnector consumer;

    /** Topic to listen from.*/
    private String topic_;

    /** The pending molecules queue.*/
    private BlockingQueue<Molecule> pendingMolecules_;

    private String serviceId_;

    /**
     * 
     * Constructor.
     * 
     * @param workflowId        WorkflowId
     * @param serviceId         ServiceId
     * @param options           Options.
     * @param pendingMolecules  Pending molecule queue.
     */
    public MoleculeListener(String workflowId,
            String serviceId, Map<String, String> options, BlockingQueue<Molecule> pendingMolecules) {
       workflowId_ = workflowId;
       serviceId_ = serviceId;
       options_ = options;
       pendingMolecules_ = pendingMolecules;
       topic_ = workflowId_ + "." + serviceId_;
       consumer = Consumer.createJavaConsumerConnector(createConsumerConfig());
    }

    /**
     * 
     * Creates the consumer configs required by Kafka.
     *  
     * @return  the Consumer Config.
     */
    private ConsumerConfig createConsumerConfig() {
        Properties props = new Properties();
        props.put("zookeeper.connect", options_.get(Options.KAFKA_ZOOKEEPER_CONNECT));
        props.put("group.id", UUID.randomUUID().toString());
        props.put("zk.sessiontimeout.ms", "400");
        props.put("zk.synctime.ms", "200");
        props.put("autocommit.interval.ms", "1000");
        // we start consuming from the "beginning"
        props.put("auto.offset.reset", "smallest");

        return new ConsumerConfig(props);
    }

    /* (non-Javadoc)
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {
        Map<String, Integer> topicCountMap = new HashMap<String, Integer>();
        topicCountMap.put(topic_, new Integer(1));
       
        Map<String, List<KafkaStream<String, Object>>> consumerMap = consumer.createMessageStreams(
                topicCountMap,
                new StringDecoder(null),
                new ObjectDecoder());
        
        KafkaStream<String, Object> streams = consumerMap.get(topic_).get(0);
        
        ConsumerIterator<String, Object> it = streams.iterator();
        while (it.hasNext()) {
            MessageAndMetadata<String, Object> m = it.next();
            pendingMolecules_.add(((Molecule) m.message()));
        }
    }

    public void close() {
       consumer.shutdown();
    }

}
