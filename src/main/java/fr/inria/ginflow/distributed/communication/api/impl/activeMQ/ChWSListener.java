/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.distributed.communication.api.impl.activeMQ;

import java.util.concurrent.BlockingQueue;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.hocl.core.hocli.HocliWorkflow;
import fr.inria.hocl.core.hocli.Molecule;

/**
 * 
 * Molecule receiver on the service executor side. Received molecules are
 * queued, waiting the next reduction (this avoid to reduce every time a new
 * molecule arrives)
 * 
 * @author msimonin, hfernandez
 */
public class ChWSListener implements MessageListener {

    /** The logger. */
    final static Logger log = LoggerFactory.getLogger(ChWSListener.class);

    private Connection connection;
    private Session session;
    private String name;

    private BlockingQueue<Molecule> pendingMolecules_;

    /** This is the topic used to publish molecule from the service invoker. */
    private String destinationString;

    public ChWSListener(Connection connection, String name,
            BlockingQueue<Molecule> pendingMolecules) {

        this.connection = connection;
        this.name = name;
        this.destinationString = HocliWorkflow.workflowId + "." + this.name;
        this.pendingMolecules_ = pendingMolecules;
        log.debug(String.format("ChWSListener created \n" + "Name : %s \n"
                + "ServiceTopic : %s \n", name, destinationString));
    }

    public void run() throws Exception {

        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Destination queue = session.createQueue(destinationString);
        MessageConsumer consumer = session.createConsumer(queue);
        
        consumer.setMessageListener(this);

        log.debug("Waiting for messages on topic " + destinationString);
    }

    /**
     * Routine called upon receive.
     * 
     */
    public void onMessage(Message message) {
        try {
            log.debug("Message received");
            if (message instanceof TextMessage) {
                TextMessage text = (TextMessage) message;
                log.debug("Received a TextMessage " + text.getText());
            } else {
                log.debug("Received a new molecule");
                ObjectMessage obj = (ObjectMessage) message;
                Molecule mol = (Molecule) obj.getObject();
                log.debug("ChWSListener received molecule." + mol.toString());

                /** Protect the solution. */
                // optimisation batch add molecule.
                // and reduce : don't block here
                // pendingMolecule.add(mol)
                // in a separate thread ()
                pendingMolecules_.add(mol);

                // synchronized (lockObject) {
                // sol.addMolecule(mol);
                // ReduceSolutionFlag.reduce = true;
                // //notify the service invoker routine.
                // lockObject.notify();
                // }

            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }

    }

}
