/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.distributed.communication.api.impl.kafka;

import java.util.Map;

import fr.inria.ginflow.distributed.communication.api.CentralMultisetUpdateListener;
import fr.inria.ginflow.distributed.communication.api.impl.activeMQ.ActiveMQCentralMultisetUpdateListener;
import fr.inria.ginflow.exceptions.CommunicationException;
import fr.inria.ginflow.listener.ServiceListener;
import fr.inria.ginflow.listener.WorkflowListener;

public class KafkaCentralMultisetUpdateListener extends CentralMultisetUpdateListener {
	 /** Thread listener.*/
    private UpdateWorkflowListener updateWorkflowListener_;

    /**
     * 
     * Constructor.
     * 
     * @param workflowId    The workflowId.
     * @param options       The options.
     * @param listener      The listener.
     */
    public KafkaCentralMultisetUpdateListener(String workflowId,
            Map<String, String> options, WorkflowListener listener) {
        super(workflowId, options, listener);
        updateWorkflowListener_ = new UpdateWorkflowListener(workflowId, options, listener);
    }

    @Override
    public void listen() {
    	updateWorkflowListener_.start();
        
    }

    @Override
    public void close() throws CommunicationException {
    	updateWorkflowListener_.close();
    }

}
