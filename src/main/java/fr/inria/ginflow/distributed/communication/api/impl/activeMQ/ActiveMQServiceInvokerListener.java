/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.distributed.communication.api.impl.activeMQ;

import java.util.Map;
import java.util.concurrent.BlockingQueue;

import javax.jms.Connection;
import javax.jms.JMSException;

import org.apache.activemq.ActiveMQConnectionFactory;

import fr.inria.ginflow.distributed.communication.api.ServiceInvokerListener;
import fr.inria.ginflow.exceptions.CommunicationException;
import fr.inria.ginflow.options.Options;
import fr.inria.hocl.core.hocli.Molecule;

public class ActiveMQServiceInvokerListener extends ServiceInvokerListener {
    
    /** Connection to ActiveMQ.*/
    private Connection connection_;

    /** Wrap old implementation 
     * TODO : extract it.
     * */
    private ChWSListener oChWSListener_; 
    
    /**
     * @param workflowId		The workflow id
     * @param serviceId			The service id
     * @param options			The options
     * @param pendingMolecules	Queues of molecule that have to be reduced
     */
    public ActiveMQServiceInvokerListener(String workflowId,
            String serviceId, Map<String, String> options, BlockingQueue<Molecule> pendingMolecules) {
        super(workflowId, serviceId, options, pendingMolecules);
        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(
                options_.get(Options.ACTIVEMQ_BROKER_URL));
        try {
            connection_ = factory.createConnection();
            connection_.start();
        } catch (JMSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        oChWSListener_ = new ChWSListener(connection_,
                serviceId_, pendingMolecules);
        
    }

    @Override
    public void listen() throws Exception {
        oChWSListener_.run();
        
    }

    @Override
    public void close() throws CommunicationException {
        try {
            connection_.close();
        } catch (JMSException e) {
           throw new CommunicationException(e);
        }
    }

}
