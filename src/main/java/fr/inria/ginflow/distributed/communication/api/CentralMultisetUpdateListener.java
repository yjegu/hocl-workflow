/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.distributed.communication.api;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ginflow.exceptions.CommunicationException;
import fr.inria.ginflow.listener.ServiceListener;
import fr.inria.ginflow.listener.WorkflowListener;

/**
 * @author msimonin
 * 
 */
public abstract class CentralMultisetUpdateListener {

    /** The logger. */
    final static Logger log_ = LoggerFactory
            .getLogger(CentralMultisetUpdateListener.class);

    /** The options */
    protected Map<String, String> options_;

    /** The listener. */
    protected WorkflowListener listener_;

    /** The workflowId.*/
    protected String workflowId_;

    public CentralMultisetUpdateListener(String workflowId, Map<String, String> options,
            WorkflowListener listener) {
        workflowId_ = workflowId;
        options_ = options;
        listener_ = listener;
    }

    public abstract void listen();

    public abstract void close() throws CommunicationException;
}
