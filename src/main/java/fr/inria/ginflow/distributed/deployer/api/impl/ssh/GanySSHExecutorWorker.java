/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.distributed.deployer.api.impl.ssh;

import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.ethz.ssh2.ChannelCondition;
import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.Session;
import fr.inria.ginflow.distributed.deployer.api.Command;
import fr.inria.ginflow.executor.api.impl.dist.ssh.listener.DeploymentListener;

public class GanySSHExecutorWorker implements Callable<Void> {

    /** Logger. */
    private static final Logger log_ = LoggerFactory
            .getLogger(GanySSHExecutorWorker.class);

    /** The ssh client to use. */
    private String host_;

    /** The command to run. */
    private Command command_;

    /** Join timeout. */
    private int pollingTimeout_;

    private GanySSHConnectionPool pool_;

	private DeploymentListener listener_;

    /**
     * 
     * Create a command worker.
     * 
     * @param host The sshClient to use
     * @param command The command to run
     * @param pollingTimeout  The polling Timeout
     * @param pool the connection pool
     * @param listener Deployment listener
     * 
     */
    public GanySSHExecutorWorker(String host, Command command,
            int pollingTimeout, GanySSHConnectionPool pool, DeploymentListener listener) {
        host_ = host;
        command_ = command;
        pollingTimeout_ = pollingTimeout;
        pool_ = pool;
        listener_ = listener;
    }

    @Override
    public Void call() throws Exception {
        Session sess = null;
        try {
            Connection sshClient = pool_.getConnection(host_);
            sess = sshClient.openSession();
            log_.debug(String.format("%s : Executing %s", host_, command_.getCommand()));
            sess.execCommand(command_.getCommand());
            int res = sess.waitForCondition(ChannelCondition.CLOSED,
                    pollingTimeout_);
            if ((res & ChannelCondition.TIMEOUT) != 0) {
                String message = String.format("%s : Timeout occured %d ",
                        host_, pollingTimeout_);
                log_.error(message);
                throw new Exception(message);
            }
        } catch (Exception e) {
            String message = String.format("Remote command execution failed on %s", host_);
            log_.error(message);
            throw new Exception(e);
        } finally {
            // the session maybe null in case of connection issue.
            // e.g unknown host 
            if (sess != null) {
                log_.debug("Closing ssh session");
                sess.close();
            }
            notifySuccess();
            
        }
        return null;
    }

	private void notifySuccess() {
		if (listener_ != null) {
			listener_.notifyDeploymentSuccess(command_);
		}
		
	}

}
