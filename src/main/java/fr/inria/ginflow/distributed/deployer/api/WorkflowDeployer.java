/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.distributed.deployer.api;

import fr.inria.ginflow.exceptions.GinFlowExecutorException;
import fr.inria.ginflow.internal.api.Workflow;
import fr.inria.ginflow.listenable.WorkflowListenable;

public interface WorkflowDeployer extends WorkflowListenable {

	 
	/**
	 * 
	 * Deploys synchronously the workflow.
	 * 
	 * 
	 * @param workflow	The workflow to deploy.
	 * @throws GinFlowExecutorException GinFlowExecutorException 
	 */
	void deploy(Workflow workflow) throws GinFlowExecutorException;

	
	/**
	 * Clean the execution of the workflow.
	 */
	void clean();

	
}