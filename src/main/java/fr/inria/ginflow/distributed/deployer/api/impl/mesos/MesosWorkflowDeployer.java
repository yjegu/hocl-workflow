/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.distributed.deployer.api.impl.mesos;

import java.util.Map;

import org.apache.mesos.MesosSchedulerDriver;
import org.apache.mesos.Protos.FrameworkInfo;
import org.apache.mesos.Protos.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ginflow.distributed.deployer.api.Command;
import fr.inria.ginflow.distributed.deployer.api.WorkflowDeployer;
import fr.inria.ginflow.exceptions.GinFlowExecutorException;
import fr.inria.ginflow.executor.api.impl.dist.ssh.listener.DeploymentListener;
import fr.inria.ginflow.internal.api.Workflow;
import fr.inria.ginflow.listener.WorkflowListener;
import fr.inria.ginflow.options.Options;

public class MesosWorkflowDeployer implements WorkflowDeployer, DeploymentListener {
    /** The logger. */
    final static Logger log = LoggerFactory.getLogger(MesosWorkflowDeployer.class);
    
	private MesosSchedulerDriver driver_;
	private String master_;
	private String user_;
	private Map<String, String> options_;
	private MesosGinflowScheduler scheduler_;

	private Workflow workflow_;

	private WorkflowListener workflowListener_;

	public MesosWorkflowDeployer(Workflow workflow, Map<String, String> options) {
		workflow_ = workflow;
        master_ = options.get(Options.MESOS_MASTER);
        user_ = options.get(Options.MESOS_USER);
        options_ = options;
        FrameworkInfo.Builder frameworkBuilder = FrameworkInfo.newBuilder()
                .setName("GinflowFramework").setUser(user_);
        scheduler_ = new MesosGinflowScheduler(workflow_, options, this);
        driver_ = new MesosSchedulerDriver(scheduler_, frameworkBuilder.build(),
                master_);
        
        driver_.start();
        
	}

	@Override
	public void deploy(Workflow workflow) throws GinFlowExecutorException {
		 String deploymentId = scheduler_.deploy(workflow);
		 boolean isDeployed = false;
		 // long polling
		 while(!isDeployed) {
			 try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				throw new GinFlowExecutorException("Timed out when polling deployment");
			}
			 isDeployed = scheduler_.isDeployed(deploymentId);
			 log.debug("Workflow is deployed {}", isDeployed);
		 }
	}

	@Override
	public void clean() {
		driver_.stop();
	}

	@Override
	public void setWorkflowListener(WorkflowListener workflowListener) {
		workflowListener_ = workflowListener;
		
	}

	@Override
	public void notifyDeploymentSuccess(Command command) {
		workflowListener_.onServiceDeployed(command.getId());
	}

}
