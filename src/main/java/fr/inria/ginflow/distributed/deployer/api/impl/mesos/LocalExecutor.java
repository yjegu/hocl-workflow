/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.distributed.deployer.api.impl.mesos;

import org.apache.mesos.Executor;
import org.apache.mesos.ExecutorDriver;
import org.apache.mesos.MesosExecutorDriver;
import org.apache.mesos.Protos.ExecutorInfo;
import org.apache.mesos.Protos.FrameworkInfo;
import org.apache.mesos.Protos.SlaveInfo;
import org.apache.mesos.Protos.Status;
import org.apache.mesos.Protos.TaskID;
import org.apache.mesos.Protos.TaskInfo;
import org.apache.mesos.Protos.TaskState;
import org.apache.mesos.Protos.TaskStatus;

import fr.inria.ginflow.executor.api.impl.dist.ServiceInvoker;
import fr.inria.hocl.core.hocli.HocliWorkflow;

/**
 * 
 * Ginflow executor wraps the service agent.
 * 
 * @author msimonin
 * 
 */
public class LocalExecutor implements Executor {

    private ServiceInvoker serviceInvoker_;

    public LocalExecutor(String workflowId, String serviceId, String[] args)
            throws Exception {
        serviceInvoker_ = new ServiceInvoker(workflowId, serviceId, args);

    }

    public void disconnected(ExecutorDriver arg0) {
        // TODO Auto-generated method stub

    }

    public void error(ExecutorDriver arg0, String arg1) {
        // TODO Auto-generated method stub

    }

    public void frameworkMessage(ExecutorDriver arg0, byte[] arg1) {
        // TODO Auto-generated method stub

    }

    public void killTask(ExecutorDriver arg0, TaskID arg1) {
        // TODO Auto-generated method stub

    }

    public void launchTask(ExecutorDriver arg0, TaskInfo arg1) {
        // TODO Auto-generated method stub

    }

    public void registered(ExecutorDriver arg0, ExecutorInfo arg1,
            FrameworkInfo arg2, SlaveInfo arg3) {
        // TODO Auto-generated method stub

    }

    public void reregistered(ExecutorDriver arg0, SlaveInfo arg1) {
        // TODO Auto-generated method stub

    }

    public void shutdown(ExecutorDriver arg0) {
        // TODO Auto-generated method stub

    }

    // TODO wrap logic.
    public static void main(String[] args) throws Exception {
        HocliWorkflow.init(args);
        LocalExecutor executor = new LocalExecutor(
                HocliWorkflow.workflowId, HocliWorkflow.serviceId, args);

        MesosExecutorDriver driver = new MesosExecutorDriver(executor);
        // register the executor
        driver.start();
        // send ready state ? need task id.
        TaskStatus status = TaskStatus
                .newBuilder()
                .setState(TaskState.TASK_RUNNING)
                .setTaskId(
                        TaskID.newBuilder().setValue(HocliWorkflow.serviceId)
                                .build()).build();
        driver.sendStatusUpdate(status);
        // start it (here wait for incoming solution to reduce)
        executor.start();
        System.exit(driver.run() == Status.DRIVER_STOPPED ? 0 : 1);

    }

    private void start() throws Exception {
        serviceInvoker_.start();
    }

}
