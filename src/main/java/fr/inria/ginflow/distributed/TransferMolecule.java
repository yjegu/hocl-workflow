/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.distributed;

import java.util.ArrayList;
import java.util.List;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.Topic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ginflow.distributed.communication.CommunicationType;
import fr.inria.ginflow.distributed.communication.api.SimplePublisher;
import fr.inria.hocl.core.hocli.Atom;
import fr.inria.hocl.core.hocli.ExternalObject;
import fr.inria.hocl.core.hocli.HocliWorkflow;
import fr.inria.hocl.core.hocli.Molecule;
import fr.inria.hocl.core.hocli.SimpleIterator;

/**
 * 
 * Transfer a molecule to a destination
 * 
 * @author hfernandez, msimonin
 * 
 */
public class TransferMolecule {

    /** The logger. */
    final static Logger log = LoggerFactory.getLogger(TransferMolecule.class);

    private static Connection connection;
    
    /** Origin of the Molecule transfered*/
    private static String origen;
    
    /** The workflowId.*/
    private static String workflowId;

    private static SimplePublisher serviceInvokerPublisher_;

    public static void setConnection(Connection con) {
        connection = con;
        log.debug("connection set " + connection);
    }

    public static void setOrigen(String origen) {
        TransferMolecule.origen = origen;
    }
    
    public static void setWorkflowId(String workflowId) {
        TransferMolecule.workflowId = workflowId;
    }
    
    public static void setPublisher(
            SimplePublisher serviceInvokerPublisher) {
        serviceInvokerPublisher_ = serviceInvokerPublisher;
        
    }

    public static boolean put(String t, Molecule destinations, String key) {
        log.debug(String
                .format("Transfer one key to a set of destinations"));
        List<String> dests = new ArrayList<String>();
        SimpleIterator<Atom> iterator = destinations.newIterator();
        Atom atomIterator;
        while ((atomIterator = iterator.next()) != null) {
            if (atomIterator instanceof ExternalObject) {
                ExternalObject external = (ExternalObject) atomIterator;
                if (external.getObject() instanceof String) {
                    String destination = (String) external.getObject();
                    dests.add(destination);
                }
            }
        }

        try {
            ExternalObject extKey = new ExternalObject(key);
            Molecule molecule = new Molecule();
            molecule.add(extKey);
            log.debug("Size: " + molecule.size() + " containing: "
                    + molecule.toString());
            for (String dest : dests) {
                log.debug(String.format("Transfer one molecule to %s", dest));
                serviceInvokerPublisher_.publish(molecule, CommunicationType.fromServiceQueue(workflowId, dest));
            }
        } catch (Exception e) {
            log.error("Error while transferring molecule", e);
            return false;
        }
        return true;
    }

    public static boolean put(String destination, Molecule molecule) {
        try {
            log.debug(String.format("Transfer one molecule %s to a a single destination : %s", molecule, destination));
            // the molecule is cloned here to allow to be captured by another reaction during activemq serialization.
            serviceInvokerPublisher_.publish(molecule.clone(), CommunicationType.fromServiceQueue(workflowId, destination));
        } catch (Exception e) {
            log.error("RemoteIOclient exception: " + e);
            e.printStackTrace();
            return false;
        }
        return true;
    }

    

}
