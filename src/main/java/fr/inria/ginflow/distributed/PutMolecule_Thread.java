/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.distributed;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.inria.ginflow.executor.api.impl.dist.CentralMultiset;
import fr.inria.ginflow.internal.api.GinflowType;
import fr.inria.ginflow.internal.api.Service;
import fr.inria.ginflow.internal.api.Workflow;
import fr.inria.hocl.core.hocli.Molecule;
import fr.inria.hocl.core.hocli.Solution;
import fr.inria.hocl.core.hocli.Tuple;

/**
 * 
 * This thread is responsible for putting service description back in the
 * workflow description. The choice to run it in the background has been made
 * originally.
 * 
 * @author msimonin, hfernandez
 * 
 */
public class PutMolecule_Thread extends Thread {
    
    /** The logger.*/
    final static Logger log = LoggerFactory.getLogger(PutMolecule_Thread.class);
    
	
	private String mol_id, origen;
	private Molecule mol;

	/** The workflow.*/ 
    private Workflow workflow_;


    private CentralMultiset listener_;
	
	public PutMolecule_Thread(Workflow workflow,Molecule mol, String mol_id, String origen, CentralMultiset listener){
		super();
		workflow_ = workflow;
		this.mol_id=mol_id;
		this.mol=mol;
		this.origen=origen;
		listener_ = listener;
	}
	
	/**
	 * 
	 * TODO : use the new API.
	 * 
	 * @param chWS_id
	 * @param mol
	 * @param origen
	 */
	private synchronized Service  updateChWS(String chWS_id, Molecule mol, String origen) {
		// get the service 
		// Contrary to centralized execution, here we get the original service.
		Service service = workflow_.getService(chWS_id);
		Solution solution = new Solution();
		solution.addMolecule(mol);
		service.setValue(solution);
		// The following is not needed.
		//workflow_.setService(key, solution)
		//log.debug("(after) sol content" + workflow_);
		return service;
		
	}
	
	public void run(){
		log.debug("START PutMolecule THREAD for {} / {}", origen, mol_id);
			//filterMainContainerMols(mol);
		Service service = updateChWS(mol_id, mol,origen);
		// They are two types of results molecule 
		// RES is a molecule thaht is generated alongside a SEND
		// RESULT is generated alone (the result is blocked)
		// TODO why do we need two ?
		Tuple res = service.has(GinflowType.RES);
		if (res != null) {
		    listener_.onResultReceived(service);
		}
		Tuple result = service.has(GinflowType.RESULT);
		if (result != null) {
			listener_.onResultReceived(service);
		}
		
//		log.debug(mainSolution.toString());
		log.debug("END PutMolecule THREAD for {} / {}", origen, mol_id);
		
	}
}
