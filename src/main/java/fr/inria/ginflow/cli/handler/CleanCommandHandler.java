/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.cli.handler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ch.qos.logback.core.joran.spi.JoranException;
import fr.inria.ginflow.cli.command.CleanCommand;
import fr.inria.ginflow.distributed.deployer.api.Command;
import fr.inria.ginflow.distributed.deployer.api.impl.ssh.GanySshRemoteExecutor;
import fr.inria.ginflow.distributed.deployer.api.impl.ssh.RemoteExecutor;
import fr.inria.ginflow.exceptions.GinFlowExecutorException;
import fr.inria.ginflow.exceptions.GinflowException;
import fr.inria.ginflow.executor.api.impl.dist.ssh.listener.DeploymentListener;
import fr.inria.ginflow.options.OptionException;
import fr.inria.ginflow.options.Options;

/**
 * 
 * Command handler : launch the workflow.
 * 
 * @author msimonin
 * 
 */
public class CleanCommandHandler extends CommonCommandHandler implements DeploymentListener {

    /** The logger. */
    final static Logger log = LoggerFactory.getLogger(CleanCommandHandler.class);

    /** Parsed command. */
    private CleanCommand cleanCommand_;

    public CleanCommandHandler(CleanCommand cleanCommand) {
        super(cleanCommand);
        cleanCommand_ = cleanCommand;
    }

    /**
     * Execute the command - Read the json file - create the workflow - create
     * the relevant executor - launch the workflow - retrieve results
     * 
     * @throws IOException				IOException
     * @throws JsonMappingException		JsonMappingException
     * @throws JsonParseException		JsonParseException
     * @throws GinflowException			GinflowException
     * @throws OptionException			OptionException
     * @throws JoranException			JoranException 
     * @throws GinFlowExecutorException GinFlowExecutorException 
     */
    public void dispatchCommand() throws JsonParseException,
            JsonMappingException, IOException, GinflowException,
            OptionException, JoranException, GinFlowExecutorException {

        configureLogger();
        Options options = loadOptions();
        String workflowId = cleanCommand_.getWorkflowId();
        
        log.info("Cleaning the workflow {}", workflowId);
        
        String grepCommand = String.format("LocalExecutor -w %s", workflowId);
        String killCommand = String.format("ps -ef|"
                + "grep '%s'|"
                + "grep -v grep|"
                + "awk '{print $2}'|"
                + "xargs kill" , grepCommand);
        ArrayList<String> machines = Options.extractMachines(options.get(Options.SSH_MACHINES));
        
        
        try (RemoteExecutor remoteExecutor = new GanySshRemoteExecutor(
                options.get(Options.SSH_USERNAME),
                new File(options.get(Options.SSH_PRIVATE_KEY)), 
                options.get(Options.SSH_PASSWORD),
                this
                )){
            remoteExecutor.execParallel(machines, new Command(killCommand));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }

	@Override
	public void notifyDeploymentSuccess(Command command) {
		log.info("Exiting");
		System.exit(0);
	}

    

}
