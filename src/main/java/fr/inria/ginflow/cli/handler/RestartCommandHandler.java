/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.cli.handler;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ch.qos.logback.core.joran.spi.JoranException;
import fr.inria.ginflow.cli.command.RestartCommand;
import fr.inria.ginflow.cli.command.UpdateCommand;
import fr.inria.ginflow.distributed.communication.CommunicationFactory;
import fr.inria.ginflow.distributed.communication.CommunicationType;
import fr.inria.ginflow.distributed.communication.api.SimplePublisher;
import fr.inria.ginflow.distributed.communication.embedded.api.EmbeddedBroker;
import fr.inria.ginflow.exceptions.GinFlowExecutorException;
import fr.inria.ginflow.exceptions.GinflowException;
import fr.inria.ginflow.executor.ExecutorFactory;
import fr.inria.ginflow.executor.api.GinflowExecutor;
import fr.inria.ginflow.external.api.WorkflowBuilder;
import fr.inria.ginflow.external.json.GinflowModule;
import fr.inria.ginflow.internal.api.Workflow;
import fr.inria.ginflow.listener.external.api.ExternalWorkflowListener;
import fr.inria.ginflow.options.OptionException;
import fr.inria.ginflow.options.Options;

/**
 * 
 * Command handler : update the workflow on the fly.
 * 
 * @author msimonin
 * 
 */
public class RestartCommandHandler extends CommonCommandHandler {

    /** The logger. */
    final static Logger log = LoggerFactory
            .getLogger(RestartCommandHandler.class);

    /** Parsed command. */
    private RestartCommand ginflowCommand_;

    public RestartCommandHandler(RestartCommand restartCommand) {
        super(restartCommand);
        ginflowCommand_ = restartCommand;
    }

    /**
     * Execute the command - Read the json file - create the workflow - create
     * the relevant executor - launch the workflow - retrieve results
     * 
     * @throws IOException				IOException
     * @throws JsonMappingException		JsonMappingException
     * @throws JsonParseException		JsonParseException
     * @throws GinflowException			GinflowException
     * @throws OptionException			OptionException
     * @throws JoranException			JoranException
     * @throws GinFlowExecutorException	GinFlowExecutorException
     */
    public void dispatchCommand() throws JsonParseException,
            JsonMappingException, IOException, GinflowException,
            OptionException, JoranException, GinFlowExecutorException {

        configureLogger();

        // - read the json file
        String workflowFile = ginflowCommand_.getWorkflowFile();
        String workflowId = ginflowCommand_.getWorkflowId();
        
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new GinflowModule());
        WorkflowBuilder wBuilder = mapper.readValue(new File(workflowFile),
                WorkflowBuilder.class);
        
        log.info("Initializing the workflow");
        // create the workflow and force the id to the old one.
        Workflow workflow = wBuilder.build(workflowId);
        
        log.info("Restarting workflow {}", workflowId);
        Options options = loadOptions();
        //this code is broker specific
        List<ExternalWorkflowListener> listeners = new ArrayList<ExternalWorkflowListener>();
		// If it is indicated,
		// we spin up a dedicated activeMQ(only for now) broker
		
		EmbeddedBroker broker = null;
		if (embeddedBroker(options)) {
			broker = CommunicationFactory.newEmbeddedBroker(options);
			broker.start();
		}
		
		log.info("Reexecuting the workflow");
		GinflowExecutor executor = ExecutorFactory.newExecutor(workflow, "noop", options);
        executor.restart();
        
        String jsonOutput = String.format("%s.ginout", workflowFile);
		log.info("Writing json output to {}", jsonOutput);
		String json = mapper.writeValueAsString(workflow);
		// output to a file
		try (PrintWriter out = new PrintWriter(jsonOutput))

		{
			out.write(json);
		}

		log.info("Exiting");
		if (broker != null) {
			broker.stop();
		}
		
		System.exit(0);

    }  
    
    /**
	 * 
	 * Check if the broker is embedded
	 * 
	 * @param options
	 * @return true iff the broker is embedded.
	 */
	private boolean embeddedBroker(Options options) {
		return !options.standaloneBroker();
	}

}
