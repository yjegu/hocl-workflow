/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.cli.command;

import com.beust.jcommander.Parameter;

public class CleanCommand extends CommonCommand {
    
    public static final String name = "clean";
    
    @Parameter(names = {"-w", "--workflow"},
            description = "workflow id",
            required = true
            )
    private String workflowId_;
    
    public CleanCommand() {
        super();
        // TODO Auto-generated constructor stub
    }



    /**
     * @return the workflowId
     */
    public String getWorkflowId() {
        return workflowId_;
    }
    
    

    /**
     * @param workflowId the workflowId to set
     */
    public void setWorkflowId(String workflowId) {
        workflowId_ = workflowId;
    }



    @Override
    public String getName() {
      return name;
    }



   
 
}
