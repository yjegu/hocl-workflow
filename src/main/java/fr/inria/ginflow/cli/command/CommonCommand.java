/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.cli.command;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.beust.jcommander.DynamicParameter;
import com.beust.jcommander.Parameter;

public abstract class CommonCommand {

    /** The logger. */
    final static Logger log = LoggerFactory.getLogger(CommonCommand.class);
    
    
    @Parameter(names = {"-c", "--config"},
            description = "Config file")
    private String configFile_ = "ginflow.yaml";
    
    @DynamicParameter(names = {"-o", "--option"}, description = "Executor options")
    private  Map<String, String> executorOptions_;

    @Parameter(names = {"-d", "--debug"}, validateWith = ValidDebugLevel.class)
    private int debug_ = 0;
    
    @Parameter(names = {"-h", "--help"}, help = true)
    private boolean help_;
    
    
    public CommonCommand() {
        super();
        executorOptions_ = new HashMap<String, String>();
    }
    
    public abstract String getName() ;        
      

    /**
     * @return the configFile
     */
    public String getConfigFile() {
        return configFile_;
    }

    /**
     * @param configFile the configFile to set
     */
    public void setConfigFile(String configFile) {
        configFile_ = configFile;
    }

    /**
     * @return the executorOptions
     */
    public Map<String, String> getExecutorOptions() {
        return executorOptions_;
    }

    /**
     * @param executorOptions the executorOptions to set
     */
    public void setExecutorOptions(Map<String, String> executorOptions) {
        executorOptions_ = executorOptions;
    }

    /**
     * @return the debug
     */
    public int getDebug() {
        return debug_;
    }

    /**
     * @param debug the debug to set
     */
    public void setDebug(int debug) {
        debug_ = debug;
    }

    /**
     * @return the help
     */
    public boolean isHelp() {
        return help_;
    }

    /**
     * @param help the help to set
     */
    public void setHelp(boolean help) {
        help_ = help;
    }
    
}
