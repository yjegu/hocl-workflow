/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.cli.command;

import com.beust.jcommander.Parameter;


/**
 * 
 * Description of the command line interface.
 * 
 * @author msimonin
 *
 */
public class LaunchCommand extends CommonCommand {

    public static final String name = "launch";

    @Parameter(names = {"-w", "--workflow"},
            description = "Path to the json file containing the Workflow definition",
            required = true
            )
    private String workflowFile_;
    
    @Parameter(names = {"-e", "--executor"},
            description = "Executor to use, possible values are : central, mesos, ssh"
            )
    private String executorName_ = "central";
    
    @Parameter(names = {"--web"},
    		description = "Enables web ui"
    		)
    private boolean webEnabled_ = false;
    
 


	public LaunchCommand() {
        super();
    }


    /**
     * @return the workflowFile
     */
    public String getWorkflowFile() {
        return workflowFile_;
    }

    /**
     * @param workflowFile the workflowFile to set
     */
    public void setWorkflowFile(String workflowFile) {
        workflowFile_ = workflowFile;
    }

    /**
     * @return the executorName
     */
    public String getExecutorName() {
        return executorName_;
    }

    /**
     * @param executorName the executorName to set
     */
    public void setExecutorName(String executorName) {
        executorName_ = executorName;
    }


    @Override
    public String getName() {
        return name;
    }

    public boolean isWebEnabled() {
 		return webEnabled_;
 	}


        
}