/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.executor.api;

import java.util.ArrayList;
import java.util.List;

import fr.inria.ginflow.exceptions.GinFlowExecutorException;
import fr.inria.ginflow.executor.api.impl.DistributedWorkflowModifier;
import fr.inria.ginflow.executor.api.impl.WorkflowModifier;
import fr.inria.ginflow.internal.api.GinflowTuple;
import fr.inria.ginflow.internal.api.Service;
import fr.inria.ginflow.internal.api.Workflow;
import fr.inria.ginflow.listener.WorkflowListener;
import fr.inria.ginflow.listener.external.api.ExternalWorkflowListener;

/**
 * 
 * API for GinflowExecutor.
 * 
 * @author msimonin
 * 
 */
public abstract class  GinflowExecutor implements WorkflowListener {

	/** The different executor. */
	public static final String CENTRAL = "central";
	public static final String MESOS = "mesos";
	public static final String SSH = "ssh";
	public static final String NOOP = "noop";
	
	/** The listeners. */
	protected List<ExternalWorkflowListener> listeners_ = new ArrayList<ExternalWorkflowListener>();
	
	/**
	 * 
	 * Execute the workflow.
	 * 
	 * @throws GinFlowExecutorException	GinflowExecutorException
	 */
	public abstract void execute() throws GinFlowExecutorException;

	/**
	 * 
	 * Update a workflow
	 * 
	 * @param workflowUpdate  the workflow update (diff with rebranching information).
	 * @throws GinFlowExecutorException		GinflowExecutorException
	 */
	public abstract void update(Workflow workflowUpdate) throws GinFlowExecutorException;

	public abstract void restart() throws GinFlowExecutorException;
	
	public void addListener(ExternalWorkflowListener listener) {
		listeners_.add(listener);
	}
	

	public void addListeners(List<ExternalWorkflowListener> listeners) {
		listeners_.addAll(listeners);
	}
	
	@Override
	public void serviceDescriptionSent(GinflowTuple service) {
		for (ExternalWorkflowListener listener : listeners_) {
			listener.notifyServiceDescriptionSend(service);
		}
	}
	
	@Override
	public void onWorkflowUpdated(Workflow updateWorkflow) throws GinFlowExecutorException {
		update(updateWorkflow);
		for (ExternalWorkflowListener listener : listeners_) {
			listener.notifyWorkflowUpdate(updateWorkflow);
		}
	}

	@Override
	public void onWorkflowTerminated() {
		for (ExternalWorkflowListener listener : listeners_) {
			listener.notifyWorkflowTerminated();
		}
	}


	@Override
	public void onResultReceived(Service service) {
		for (ExternalWorkflowListener listener : listeners_) {
			listener.notifyResultReceived(service);
		}
	}
	
	@Override
	public void onServiceDeployed(String sName) {
		for (ExternalWorkflowListener listener : listeners_) {
			listener.notifyDeployed(sName);
		}
	}

	
}