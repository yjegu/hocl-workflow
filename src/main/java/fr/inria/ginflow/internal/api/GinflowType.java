/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.internal.api;

import fr.inria.hocl.core.hocli.ExternalObject;

/**
 * 
 * Some predefined keys.
 * 
 * @author msimonin
 * 
 */
public class GinflowType {

    /** Source key (data). */
    public static final String SRC = "SRC";
    
    /** Source key (control).*/
    public static final String SRC_CONTROL = "SRC_CONTROL";
    
    /** Destination key. */
    public static final String DST = "DST";
    
    /** Destination key. */
    public static final String DST_CONTROL = "DST_CONTROL";

    /** In key. */
    public static final String IN = "IN";

    /** Error key. */
    public static final String ERROR = "ERR";

    /** Result key. */
    public static final String RESULT = "RESULT";
    /** Res key.*/
    public static final String RES = "RES";

    /** Srv key = command. */
    public static final String SRV = "SRV";

    /** Name. */
    public static final String NAME = "NAME";

    /** Env. */
    public static final String ENV = "ENV";

    /** Adapt molecule.*/
    public static final String ADAPT = "ADAPT";
    public static final String ADAPT_SRC = "ADAPT_SRC";
    public static final String ADAPT_DST = "ADAPT_DST";
    public static final String ADAPT_SRC_CONTROL = "ADAPT_SRC_CONTROL";
    public static final String ADAPT_DST_CONTROL = "ADAPT_DST_CONTROL";
    
    /** results of an invocation. */
    public static final String TRANSFER = "TRANSFER";
    public static final String OUT = "OUT";
    public static final String EXIT = "EXIT";
    public static final String ERR = "ERR";

    public static final String UNKNOWN = "UNKNOWN";

	


    

    

    

}
