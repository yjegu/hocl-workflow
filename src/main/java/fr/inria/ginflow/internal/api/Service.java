/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.internal.api;

import java.util.Set;

import fr.inria.hocl.core.hocli.Atom;
import fr.inria.hocl.core.hocli.ReactionRule;
import fr.inria.hocl.core.hocli.Solution;
import fr.inria.hocl.core.hocli.SolutionObserver;
import fr.inria.hocl.core.hocli.Tuple;

/**
 * 
 * Source "SRC":&lt;src1, ..., srcn&gt;
 * 
 * @author msimonin
 * 
 */
public class Service extends GinflowTuple implements SolutionObserver {

    /** Default serial id. */
    private static final long serialVersionUID = 1L;

    /** Index used when checking stdout in the result. */
    public static final int OUT_INDEX = 0;
    /** Index used when checking stderr in the result. */
    public static final int ERR_INDEX = 1;
    /** Index used when checking the exit code in result. */
    public static final int EXIT_INDEX = 2;

    private String name_;

    /**
     * 
     * Build a service.
     * Initialized with a minimal set of tuples (SRC/DST...)
     * 
     * @param name
     *            The name of the service
     * @return the service.
     */
    public static Service newService(String name) {
        return new Service(name, true);
    }
    
    public static Service newEmptyService(String name) {
    	return new Service(name, false);
    }
    
    /**
     * 
     * Build an image of the service description known as a tuple.
     * 
     * @param serviceTuple	The service tuple
     * @return the current service
     */
    public static Service newService(Tuple serviceTuple) {
        return new Service(serviceTuple);
    }

    private Service(String name, boolean initialize) {
        super(name);
        name_ = name;
        value_.setNonInert();
        if (initialize) {
        	initialize(name);
        }

    }

    private Service(Tuple serviceTuple) {
        this(serviceTuple, true);
    }
    
    /**
     * Allows to build a really empty service (just the name is set)
     * 
     * @param serviceTuple
     * @param initialize
     */
    private Service(Tuple serviceTuple, boolean initialize) {
        super(serviceTuple);
        value_.setNonInert();
        if (initialize) {
        	initialize((String) key_.getObject());
        }
    }

    private void initialize(String name) {
        setSource(Source.newSource());
        setSourceControl(SourceControl.newSourceControl());
        setDestination(Destination.newDestination());
        setDestinationControl(DestinationControl.newDestinationControl());
        setIn(In.newIn());
        setEnv(Env.newEnv());
        // name is mandatory for distributed examples.
        setGinflowTuple(Name.newName(name));
    }

    public GinflowTuple addRule(ReactionRule rule) {
        this.addAtom(rule);
        return this;
    }

    /**
     * 
     * Sets the source (data) tuple to the service : "SRC":&lt;src1, src2 ...&gt;
     * Idempotent operation : remove existing one.
     * 
     * O(size value)
     * 
     * @param source
     *            The source tuple
     * @return the current service
     */
    public Service setSource(Source source) {
        return setGinflowTuple(source);
    }

    /**
     * 
     * Sets the source (control) tuple to the service : "SRC":&lt;src1, src2 ...&gt;
     * Idempotent operation : remove existing one.
     * 
     * O(size value)
     * 
     * @param source
     *            The source tuple
     * @return the current service
     */
    public Service setSourceControl(SourceControl source) {
        return setGinflowTuple(source);
    }

    
    /**
     * 
     * Sets the dest (data) tuple to the service : "DST":&lt;src1, src2 ...&gt; Idempotent
     * operation : remove existing one.
     * 
     * O(size value)
     * 
     * @param destination
     *            The destination tuple
     * @return the current service
     */
    public Service setDestination(Destination destination) {
        return setGinflowTuple(destination);
    }
    
    /**
     * 
     * Sets the dest (control) tuple to the service : "DST":&lt;src1, src2 ...&gt; Idempotent
     * operation : remove existing one.
     * 
     * O(size value)
     * 
     * @param destination
     *            The destination tuple
     * @return the current service
     */
    public Service setDestinationControl(DestinationControl destination) {
        return setGinflowTuple(destination);
    }
    
    public Service setIn(In in) {
        return setGinflowTuple(in);
    }

    public Service setCommand(Command command) {
        return setGinflowTuple(command);
    }

    public GinflowTuple setEnv(Env env) {
        return setGinflowTuple(env);
    }

    public void setName(String name) {
        setKey(name);
        setGinflowTuple(Name.newName(name));
    }
    
    public GinflowTuple setAdapt(Adapt adapt) {
        return setGinflowTuple(adapt);
    }
    
    public GinflowTuple setAdaptSrc(AdaptSrc adaptSrc) {
        return setGinflowTuple(adaptSrc);
    }
    
    public GinflowTuple setAdaptSrcControl(AdaptSrcControl adaptSrcControl) {
        return setGinflowTuple(adaptSrcControl);
    }

    public GinflowTuple setAdaptDst(AdaptDst adaptDst) {
        return setGinflowTuple(adaptDst);
    }

    public GinflowTuple setAdaptDstControl(AdaptDstControl adaptDstControl) {
        return setGinflowTuple(adaptDstControl);
    }

    /**
     * 
     * Sets an arbitrary ginflow tuple to the value : "Key":&lt;v1, v2 ...&gt;
     * Idempotent operation : remove existing one. NOTE: Must be used instead of
     * addValue for custom ginflowTuple
     * 
     
     * O(size value)
     * 
     * @param tuple The underlying tuple
     * @return the current service
     */
    public Service setGinflowTuple(GinflowTuple tuple) {
        Tuple innerTuple = has(tuple.getKey());
        if (innerTuple != null) {
            remove(innerTuple);
        }
        addAtom(tuple);
        return this;
    }

    /**
     * 
     * Get the results tuple if present.
     * 
     * @return The out, err, exit in an array.
     */
    @SuppressWarnings("unused")
	public GinflowTuple[] getResult() {

    	Tuple tupleRes = null;
        Tuple tupleRes1 = this.has(GinflowType.RESULT);
        if (tupleRes1 != null) {
            tupleRes = tupleRes1;
        }

        Tuple tupleRes2 = this.has(GinflowType.RES);
        if (tupleRes2 != null) {
            tupleRes = tupleRes2;
        }
        if (tupleRes == null) {
        	return null;
        }
        
        GinflowTuple gtupleRes = new GinflowTuple(tupleRes);
        // gTupleRes -> "RES":<"TRANSFER"<>>
        GinflowTuple gTupleTransfer = new GinflowTuple(
                gtupleRes.has(GinflowType.TRANSFER));

        GinflowTuple[] results = new GinflowTuple[3];

        // TODO what if we don't have everything ?
        results[Service.OUT_INDEX] = new GinflowTuple(
                gTupleTransfer.has(GinflowType.OUT));
        results[Service.ERR_INDEX] = new GinflowTuple(
                gTupleTransfer.has(GinflowType.ERR));
        results[Service.EXIT_INDEX] = new GinflowTuple(
                gTupleTransfer.has(GinflowType.EXIT));
        return results;
    }

    public void remove(Tuple gTuple) {
        getValue().remove(gTuple);
    }

    public void notifyAdd(Solution solution, Atom atom) {
        // TODO Auto-generated method stub
    }

    public void notifyRemove(Solution solution, Atom atom) {
        // TODO Auto-generated method stub

    }

    /**
     * @return the name
     */
    public String getName() {
        return name_;
    }

}
