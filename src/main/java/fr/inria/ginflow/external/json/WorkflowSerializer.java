/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.external.json;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import fr.inria.ginflow.exceptions.GinflowException;
import fr.inria.ginflow.internal.api.GinflowTuple;
import fr.inria.hocl.core.hocli.Atom;
import fr.inria.hocl.core.hocli.SimpleIterator;
import fr.inria.hocl.core.hocli.Tuple;

/**
 * 
 * 
 * @author msimonin
 * 
 */
public class WorkflowSerializer extends JsonSerializer<Tuple> {

    @Override
    public void serialize(Tuple tuple, JsonGenerator jg,
            SerializerProvider provider) throws IOException,
            JsonProcessingException {

        boolean isGinflowTuple = GinflowTuple.isGinflowTuple(tuple);
        if (!isGinflowTuple) {
            System.out.println(tuple);
            throw new IOException(GinflowException.INVALID_GINFLOW_TUPLE);
        }

        GinflowTuple gTuple = new GinflowTuple(tuple);
        jg.writeStartObject();
        jg.writeStringField("name", ((String) gTuple.getKey().getObject()));
        jg.writeArrayFieldStart("services");
        // we iterate over the values.
        SimpleIterator<Atom> it = gTuple.getValue().newIterator();
        Atom atom;
        ServiceSerializer serviceSerializer = new ServiceSerializer();
        // iterate over the services...
        while ((atom = it.next()) != null) {
            if (GinflowTuple.isGinflowTuple(atom)) {
                // we serialize recursively.
                serviceSerializer.serialize((Tuple) atom, jg, provider);
            } else {
                // jg.writeString(atom.toString());
                continue;
            }
        }
        jg.writeEndArray(); // end of services array
        jg.writeEndObject();

    }

}
