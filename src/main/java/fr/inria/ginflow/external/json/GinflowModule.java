/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.external.json;

import org.codehaus.jackson.Version;
import org.codehaus.jackson.map.module.SimpleModule;

import fr.inria.ginflow.external.api.ServiceBuilder;
import fr.inria.ginflow.external.api.WorkflowBuilder;
import fr.inria.ginflow.internal.api.Service;
import fr.inria.ginflow.internal.api.Workflow;

/**
 * 
 * Jackson module for deserialization of workflow in JSON.
 * 
 * @author msimonin
 * 
 */
public class GinflowModule extends SimpleModule {

    public GinflowModule() {
        super("GinflowModele", new Version(1, 0, 0, null));
        this.addDeserializer(ServiceBuilder.class,
                new ServiceBuilderDeserializer(ServiceBuilder.class));
        this.addDeserializer(WorkflowBuilder.class,
                new WorkflowBuilderDeserializer(WorkflowBuilder.class));
        this.addSerializer(Service.class, new ServiceSerializer());
        this.addSerializer(Workflow.class, new WorkflowSerializer());
        // TODO Auto-generated constructor stub
    }

}
