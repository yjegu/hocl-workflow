#!/usr/bin/env ruby
#
require 'json'
require 'erb'

ENV['SRV'] ||= "echo"
ENV['IN'] ||= "1"

@horizontal = (ENV['HORIZONTAL'] || "1").to_i
@vertical = (ENV['VERTICAL'] || "1").to_i
@adapt = (ENV['ADAPT'] || "0").to_i
@srv = ENV['SRV'] 
@in  = ENV['IN']

def to_ss(x)
  "#{x}"
end

# first index
first_index = 1
first = {
      :name => [first_index.to_s],
      :srv => [@srv],
      :in => [@in],
      :src_control => [],
      :dst_control => (0..@horizontal - 1).map{|x| to_ss(x + first_index + 1)}
}

last_index = @horizontal * @vertical + 2
last = {
      :name => [last_index.to_s],
      :srv => ["#{@srv} #{last_index}"],
      :in => [],
      :src_control=>(0..@horizontal - 1).map{|x| to_ss(last_index - x - 1)},
      :dst_control => []
}
# last index
def generate_internals(horizontal, vertical, previous: 1, first: 1, last: 1,  srv: "echo", simple: true, alt: false, sup:false) 
  (0..vertical-1).map{ |i|
    (0..horizontal-1).map { |j|
      index = first + i * horizontal + j + 1
      prec = index - horizontal
      suiv = index + horizontal
      s = {
        :name => [index.to_s],
        :srv  => [srv],
        :in   => [index],
      }
      s[:sup] = 1 if sup
      s[:alt] = 1 if alt
      # fill destinations
      if (i == vertical - 1)
        s[:dst_control] = [to_ss(last)]
      else
        s[:dst_control] = [to_ss(suiv)]
      end
      # fill sources
      if (i == 0)
        s[:src_control] =[to_ss(previous)] 
      else
        s[:src_control] = [to_ss(prec)]
      end
      s
    }
  }
end

internals_simple = generate_internals(@horizontal, @vertical,previous:1, first: first_index, last: last_index, srv: "echo", simple: true, sup:@adapt) 


@first = first
@internals = internals_simple.flatten
@last = last
services = [] + @internals 
services << first
services << last

if (@adapt > 0)
#  internals_simple_alt = generate_internals(@horizontal, @vertical, previous: 1, first: last_index, last:last_index,  srv: "echo", simple: true, alt:true) 
  internals_simple_alt = generate_internals(@horizontal, 1, previous: 1, first: last_index, last:last_index,  srv: "echo", simple: true, alt:true) 
  @internals_alt = internals_simple_alt.flatten
  services += @internals_alt
end

if (@adapt != 0)
  (0..@adapt.abs - 1).each do |a|
    # chose some service to fail
    @internals[-a-2][:srv] = ["bla"]
  end

end



workflow = {
  :name => "wf-#{@horizontal}-#{@vertical}",
  :services => services
}

puts JSON.pretty_generate(workflow)

