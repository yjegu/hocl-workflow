/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.options;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import static junit.framework.Assert.assertEquals;

import org.testng.Assert;
import org.testng.annotations.Test;

@Test(groups = { "main" })
public class TestOptions {
    
    @Test(enabled = true)
    public void test_options() throws OptionException {
        Options options = new Options();
        // test the default values.
        Assert.assertEquals(options.get(Options.MESOS_MASTER), "localhost:5050");
        Assert.assertEquals(options.get(Options.MESOS_USER), "mesos");
        Assert.assertEquals(options.get(Options.ACTIVEMQ_BROKER_URL), "tcp://localhost:61616");
    }
    @Test(enabled = true)
    public void test_options_environment () throws OptionException {
    	Map<String, String> env = new HashMap<String, String>();
    	env.put("GINFLOW_BLA_BLI", "1");
    	Options options = new Options();
    	options.updateWithEnvironment(env);
    	//
    	Assert.assertEquals(options.get("bla.bli"), "1");
    }
    
    @Test(enabled = true)
    public void test_options_override() throws OptionException {
        Map<String, String> opts = new HashMap<>();
        // Override some default options.
        opts.put(Options.MESOS_MASTER, "foo:5050");
        opts.put(Options.DISTRIBUTION_HOME, "foo.jar");
        Options options = new Options(opts);
        // test the default values.
        Assert.assertEquals(options.get(Options.MESOS_MASTER), "foo:5050");
        Assert.assertEquals(options.get(Options.DISTRIBUTION_HOME), "foo.jar");
    }
    
    @Test(enabled = true)
    public void testExtractMachines() {
        String string = "12.34.56.788, 34.56.777.5,locahost,lyon1.grid500.fr ,127.0.0.1";
        ArrayList<String> result = Options.extractMachines(string);
        ArrayList<String> expResult = new ArrayList<>(Arrays.asList("12.34.56.788", "34.56.777.5", "locahost", "lyon1.grid500.fr", "127.0.0.1"));
        assertEquals(expResult, result);
    }
    
    @Test(enabled = true,  expectedExceptions = FileNotFoundException.class)
    public void testFromFileNotFound() throws FileNotFoundException, OptionException {
        Options options = Options.fromFile("not existing file");
    }
    
    @Test(enabled = true)
    public void testFromFile() throws FileNotFoundException, OptionException {
        Options options = Options.fromFile("src/test/resources/options/ginflow-test.yaml");
        assertEquals(options.get("test"), "test");
    }
}
