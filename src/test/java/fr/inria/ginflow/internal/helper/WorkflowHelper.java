/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.internal.helper;

import fr.inria.ginflow.internal.api.Command;
import fr.inria.ginflow.internal.api.Destination;
import fr.inria.ginflow.internal.api.GinflowTuple;
import fr.inria.ginflow.internal.api.Service;
import fr.inria.ginflow.internal.api.Source;
import fr.inria.ginflow.internal.api.Workflow;

public class WorkflowHelper {
    public static GinflowTuple T1() {
        return Service.newService("T1")
                .setDestination(Destination.newDestination().add("T2"))
                .setCommand(Command.newCommand("T1"));
    }

    public static GinflowTuple T2() {
        return Service.newService("T2").setSource(Source.newSource().add("T1"))
                .setDestination(Destination.newDestination().add("T3"))
                .setCommand(Command.newCommand("T2"));
    }

    public static GinflowTuple T3() {
        return Service.newService("T3").setSource(Source.newSource().add("T2"))
                .setCommand(Command.newCommand("T3"));
    }

    public static Workflow workflow1(GinflowTuple T1, GinflowTuple T2, GinflowTuple T3) {
        return Workflow.newWorkflow("Wtest")
                .registerService("T1", T1)
                .registerService("T2", T2)
                .registerService("T3", T3);
    }
}
