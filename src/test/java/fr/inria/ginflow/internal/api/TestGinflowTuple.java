/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.internal.api;

import org.testng.Assert;
import org.testng.annotations.Test;

import fr.inria.ginflow.internal.api.Destination;
import fr.inria.ginflow.internal.api.GinflowTuple;
import fr.inria.ginflow.internal.api.GinflowType;
import fr.inria.ginflow.internal.api.Source;
import fr.inria.hocl.core.hocli.ExternalObject;
import fr.inria.hocl.core.hocli.Solution;
import fr.inria.hocl.core.hocli.Tuple;

/**
 * @author msimonin
 * 
 */
@Test(groups = { "main" })
public class TestGinflowTuple {

    @Test()
    public void test_equals_src_src() {
        GinflowTuple t1 = Source.newSource();
        GinflowTuple t2 = Source.newSource();
        Assert.assertTrue(t1.equals(t2));
    }
    
    @Test()
    public void test_equals_src_dest() {
        GinflowTuple t1 = Source.newSource();
        GinflowTuple t2 = Destination.newDestination();
        Assert.assertFalse(t1.equals(t2));
    }
    
    @Test()
    public void test_equals_src_src_tuple() {
        GinflowTuple t1 = Source.newSource();
        Tuple t2 = new Tuple(2);
        t2.set(0, new ExternalObject(String.valueOf(GinflowType.SRC)));
        t2.set(1, new Solution());
        Assert.assertTrue(t1.equals(t2));
    }
    
    @Test()
    public void test_equals_src_dest_tuple() {
        GinflowTuple t1 = Source.newSource();
        Tuple t2 = new Tuple(2);
        t2.set(0, new ExternalObject(String.valueOf(GinflowType.DST)));
        t2.set(1, new Solution());
        Assert.assertFalse(t1.equals(t2));
    }
    
    @Test()
    public void test_source_empty() {
        GinflowTuple source = Source.newSource();
        Assert.assertTrue(source.size == 2);
        // test first item
        String src = (String) ((ExternalObject) source.get(0)).getObject();
        Assert.assertEquals(src, String.valueOf(GinflowType.SRC));
        // test second item
        Solution sol = (Solution) ((Solution) source.get(1));
        Assert.assertTrue(sol.size() == 0);
    }

    @Test()
    public void test_source_add() {
        GinflowTuple source = Source.newSource().add("T1");
        String src = (String) ((ExternalObject) source.get(0)).getObject();
        Assert.assertEquals(src, String.valueOf(GinflowType.SRC));
        // test second item
        Solution sol = (Solution) ((Solution) source.get(1));
        Assert.assertTrue(sol.size() == 1);
        Assert.assertTrue(sol.contains(new ExternalObject("T1")));
    }

    @Test()
    public void test_destination_empty() {
        GinflowTuple source = Destination.newDestination();
        Assert.assertTrue(source.size == 2);
        // test first item
        String dst = (String) ((ExternalObject) source.get(0)).getObject();
        Assert.assertEquals(dst, String.valueOf(GinflowType.DST));
        // test second item
        Solution sol = (Solution) ((Solution) source.get(1));
        Assert.assertTrue(sol.size() == 0);
    }

    @Test()
    public void test_destination_add() {
        GinflowTuple destination = Destination.newDestination().add("T1");
        String dst = (String) ((ExternalObject) destination.get(0)).getObject();
        Assert.assertEquals(dst, String.valueOf(GinflowType.DST));
        // test second item
        Solution sol = (Solution) ((Solution) destination.get(1));
        Assert.assertTrue(sol.size() == 1);
        Assert.assertTrue(sol.contains(new ExternalObject("T1")));
    }

//    @Test()
//    public void test_service_empty() {
//        Service service = Service.newService("T1");
//        Assert.assertTrue(service.size == 2);
//        // test first item
//        String srv = (String) ((ExternalObject) service.get(0)).getObject();
//        Assert.assertEquals(srv, "T1");
//        Assert.assertFalse(service.hasSource());
//        Assert.assertFalse(service.hasDestination());
//        Assert.assertFalse(service.hasIn());
//    }
//
//    @Test()
//    public void test_service_set_src() {
//        Service service = Service.newService("T1");
//        service.setSource(Source.newSource().add("T1").add("T2"));
//        Assert.assertTrue(service.hasSource());
//        Assert.assertTrue(service.getSource().contains("T1"));
//        Assert.assertTrue(service.getSource().contains("T2"));
//        Assert.assertFalse(service.hasDestination());
//        Assert.assertFalse(service.hasIn());
//    }
//
//    @Test()
//    public void test_service_add_src() {
//        Service service = Service.newService("T1");
//        service.addSource("T1").addSource("T2");
//
//        Assert.assertTrue(service.hasSource());
//        Assert.assertTrue(service.getSource().contains("T1"));
//        Assert.assertTrue(service.getSource().contains("T2"));
//        Assert.assertFalse(service.hasDestination());
//        Assert.assertFalse(service.hasIn());
//    }
//
//    @Test()
//    public void test_service_set_dst() {
//        Service service = Service.newService("T1");
//        service.setDestination(Destination.newDestination().add("T1").add("T2"));
//        Assert.assertFalse(service.hasSource());
//        Assert.assertTrue(service.hasDestination());
//        Assert.assertTrue(service.getDestination().contains("T1"));
//        Assert.assertTrue(service.getDestination().contains("T2"));
//        Assert.assertFalse(service.hasIn());
//    }
//
//    @Test()
//    public void test_service_add_dst() {
//        Service service = Service.newService("T1");
//        service.addDestination("T1")
//            .addDestination("T2");
//        
//        Assert.assertFalse(service.hasSource());
//        Assert.assertTrue(service.hasDestination());
//        Assert.assertTrue(service.getDestination().contains("T1"));
//        Assert.assertTrue(service.getDestination().contains("T2"));
//        Assert.assertFalse(service.hasIn());
//    }
//
//    @Test()
//    public void test_service_in() {
//        Service service = Service.newService("T1");
//        service.setIn(In.newIn().add(new String("abc")).add(new Double(123)));
//        Assert.assertFalse(service.hasSource());
//        Assert.assertFalse(service.hasDestination());
//        Assert.assertTrue(service.hasIn());
//
//    }

}
