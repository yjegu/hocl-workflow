/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.internal.api;

import org.testng.Assert;
import org.testng.annotations.Test;

import fr.inria.ginflow.internal.api.GinflowType;
import fr.inria.ginflow.internal.api.Service;
import fr.inria.ginflow.internal.api.Source;
import fr.inria.hocl.core.hocli.ExternalObject;

/**
 * @author msimonin
 *
 */
@Test(groups = { "main" })
public class TestService {

                
        @Test()
        public void test_set_source(){
            Service service = Service.newService("Stest");
            Source source1 = Source.newSource().add("T1");
            service.setSource(source1);
            Assert.assertEquals(service.has(Source.key()), source1);
        }
        
        @Test()
        public void test_set_source_add(){
            Service service = Service.newService("Stest");
            Source source1 = Source.newSource().add("T1");
            service.setSource(source1);
            Assert.assertEquals(service.has(Source.key()), source1);
        }
        
        @Test()
        public void test_remove_no_source(){
            Service service = Service.newService("Stest");
            Source source1 = Source.newSource().add("T1");
            service.remove(source1);
        }
        
        @Test()
        public void test_remove_same_source(){
            Service service = Service.newService("Stest");
            Source source1 = Source.newSource().add("T1");
            service.setSource(source1);
            Assert.assertEquals(service.has(GinflowType.SRC), source1);
            service.remove(source1);
            Assert.assertNull(service.has(new ExternalObject("key")));
        }
        
        @Test()
        public void test_set_env() {
            Service service = Service.newService("Stest");
            Env env = Env.newEnv()
                    .add("foo", "bar")
                    .add("fofo=baba");
            service.setEnv(env);
            Assert.assertEquals(service.has(GinflowType.ENV), env);
            
        }
        
}


