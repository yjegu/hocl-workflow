/**
 * This file is part of hocl-workflow.
 *
 * hocl-workflow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * hocl-workflow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with hocl-workflow.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inria.ginflow.external.json;

import java.io.IOException;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.SerializableString;
import org.codehaus.jackson.io.SerializedString;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.testng.Assert;
import org.testng.annotations.Test;

import fr.inria.ginflow.internal.api.GinflowTuple;
import fr.inria.ginflow.internal.api.GinflowType;
import fr.inria.ginflow.internal.api.Service;
import fr.inria.ginflow.internal.api.Source;
import fr.inria.ginflow.internal.api.Workflow;
import fr.inria.ginflow.internal.helper.WorkflowHelper;
import fr.inria.ginflow.rules.centralised.Gw_setup;
/**
 * @author msimonin
 * 
 */
@Test(groups = { "main" })
public class TestSerialization {
    
    
    @Test(enabled = true)
    public void service_without_result() throws JsonParseException, JsonMappingException, IOException {
        GinflowTuple service = Service.newService("toto")
                .setSource(Source.newSource().add("1"));   
        
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new GinflowModule());
        String test = mapper.writeValueAsString(service);
        JsonFactory jsonFactory = new JsonFactory();
        JsonParser jp = jsonFactory.createJsonParser(test);
        
        System.out.println(test);
        jp.nextToken();
        while (jp.getCurrentToken() != JsonToken.END_OBJECT) {
            if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {
                checkGinflowTuple(jp, GinflowType.NAME.toLowerCase(), "toto");
                checkGinflowTuple(jp, GinflowType.SRC.toLowerCase(), "1");
                checkGinflowTuple(jp, GinflowType.IN.toLowerCase(), null);
                checkGinflowTuple(jp, GinflowType.DST.toLowerCase(), null);
            }
            jp.nextToken();
            
        }
    }

    @Test(enabled = true)
    public void service_rules_should_be_skipped() throws JsonParseException, JsonMappingException, IOException {
        GinflowTuple service = Service.newService("toto")
                .setSource(Source.newSource().add("1"))
                .addRule(new Gw_setup());   
        
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new GinflowModule());
        String test = mapper.writeValueAsString(service);
        JsonFactory jsonFactory = new JsonFactory();
        JsonParser jp = jsonFactory.createJsonParser(test);
        
        jp.nextToken();
        while (jp.getCurrentToken() != JsonToken.END_OBJECT) {
            if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {
                checkGinflowTuple(jp, GinflowType.NAME.toLowerCase(), "toto");
                checkGinflowTuple(jp, GinflowType.SRC.toLowerCase(), "1");
                checkGinflowTuple(jp, GinflowType.IN.toLowerCase(), null);
                checkGinflowTuple(jp, GinflowType.DST.toLowerCase(), null);
            }
            jp.nextToken();
            
        }
    }
    
    
    @Test(enabled = true)
    public void workflow() throws JsonGenerationException, JsonMappingException, IOException {
        Workflow workflow = WorkflowHelper.workflow1(WorkflowHelper.T1(), WorkflowHelper.T2(), WorkflowHelper.T3());
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new GinflowModule());
        String test = mapper.writeValueAsString(workflow);
        JsonFactory jsonFactory = new JsonFactory();
        JsonParser jp = jsonFactory.createJsonParser(test);
        
        jp.nextToken();
        while (jp.getCurrentToken() != JsonToken.END_OBJECT) {
            if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {
                checkGinflowTuple(jp, GinflowType.NAME.toLowerCase(), "Wtest");
            } else if (jp.getCurrentToken() == JsonToken.START_ARRAY) {
                // we have a service.
                // TODO
            }
            jp.nextToken();
            
        }
    }
    
    private void checkGinflowTuple(JsonParser jp, String fieldName, String value) throws JsonParseException, IOException {
        if (jp.getCurrentName() == fieldName) {
            jp.nextToken();
            Assert.assertEquals(jp.getCurrentToken(), JsonToken.START_ARRAY);
            jp.nextToken();
            if (value != null) {
                Assert.assertEquals(jp.getText(), value);
                jp.nextToken();
            }
            Assert.assertEquals(jp.getCurrentToken(), JsonToken.END_ARRAY);
        }
        
    }
    
}
